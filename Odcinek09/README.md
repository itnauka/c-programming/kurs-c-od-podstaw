# Kurs C od podstaw

### [#9 - Instrukcja continue oraz break](https://www.youtube.com/watch?v=O8aJsXodqSE&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=11)

<br />

```c
#include <sthio.h>

int main(void) {

  int liczba = 0;
  for (int i = 1; i <= 10; i++) {
    for (int j = 1; j <= 10; j++) {
      liczba = i * j;
      if ((liczba % 2) == 0) {
        printf("%*s|", 3, " - ");
        continue;
      }
      (liczba > 9) ? printf("%3d|", liczba)
                   : printf("%s%d%s|", " ", liczba, " ");
    }
    printf("\n");
  }

  return 0;
}
```

> **1 | - | 3 | - | 5 | - | 7 | - | 9 | - |**  
> **- | - | - | - | - | - | - | - | - | - |**  
> **3 | - | 9 | - | 15| - | 21| - | 27| - |**  
> **- | - | - | - | - | - | - | - | - | - |**  
> **5 | - | 15| - | 25| - | 35| - | 45| - |**  
> **- | - | - | - | - | - | - | - | - | - |**  
> **7 | - | 21| - | 35| - | 49| - | 63| - |**  
> **- | - | - | - | - | - | - | - | - | - |**  
> **9 | - | 27| - | 45| - | 63| - | 81| - |**  
> **- | - | - | - | - | - | - | - | - | - |**  

<table>
  <tr>
    <th>1</th><th>-</th><th>3</th><th>-</th><th>5</th><th>-</th><th>7</th><th>-</th><th>9</th><th>-</th>
  </tr>
  <tr>
    <th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th>
  </tr>
  <tr>
    <th>3</th><th>-</th><th>9</th><th>-</th><th>15</th><th>-</th><th>21</th><th>-</th><th>27</th><th>-</th>
  </tr>
  <tr>
    <th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th>
  </tr>
  <tr>
    <th>5</th><th>-</th><th>15</th><th>-</th><th>25</th><th>-</th><th>35</th><th>-</th><th>45</th><th>-</th>
  </tr>
  <tr>
    <th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th>
  </tr>
  <tr>
    <th>7</th><th>-</th><th>21</th><th>-</th><th>35</th><th>-</th><th>49</th><th>-</th><th>63</th><th>-</th>
  </tr>
  <tr>
    <th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th>
  </tr>
  <tr>
    <th>9</th><th>-</th><th>27</th><th>-</th><th>45</th><th>-</th><th>63</th><th>-</th><th>81</th><th>-</th>
  </tr>
  <tr>
    <th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th>
  </tr>
</table>

<br />

```c
#include <sthio.h>

int main(void) {

  liczba = 0;
  for (int i = 1; i <= 10; i++) {
    for (int j = 1; j <= 10; j++) {
      liczba = i * j;
      if ((liczba % 2) == 0) {
        printf("%*s|", 3, " - ");
        break; //<- pomija pętle wewnętrzną
      }
      (liczba > 9) ? printf("%3d|", liczba)
                   : printf("%s%d%s|", " ", liczba, " ");
    }
    printf("\n");
  }

  return 0;
}
```

> **1 | - |**  
> **- |**  
> **3 | - |**  
> **- |**  
> **5 | - |**  
> **- |**  
> **7 | - |**  
> **- |**  
> **9 | - |**  
> **- |**  

<table>
  <tr>
    <th>1</th><th>-</th>
  </tr>
  <tr>
    <th>-</th>
  </tr>
  <tr>
    <th>3</th><th>-</th>
  </tr>
  <tr>
    <th>-</th>
  </tr>
  <tr>
    <th>5</th><th>-</th>
  </tr>
  <tr>
    <th>-</th>
  </tr>
  <tr>
    <th>7</th><th>-</th>
  </tr>
  <tr>
    <th>-</th>
  </tr>
  <tr>
    <th>9</th><th>-</th>
  </tr>
  <tr>
    <th>-</th>
  </tr>
</table>

