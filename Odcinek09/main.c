// Kurs C od podstaw #9 - Instrukcja continue oraz break
// https://www.youtube.com/watch?v=O8aJsXodqSE
// https://pl.wikibooks.org/wiki/C/Operatory
// https://slideplayer.pl/slide/2905575/10/images/5/BRAMKI+LOGICZNE.jpg
// https://www.programiz.com/c-programming/c-operators

#include <stdio.h>

int main(void) {

  int liczba = 0;
  for (int i = 1; i <= 10; i++) {
    for (int j = 1; j <= 10; j++) {
      liczba = i * j;
      if ((liczba % 2) == 0) {
        printf("%*s|", 3, " - ");
        continue;
      }
      (liczba > 9) ? printf("%3d|", liczba)
                   : printf("%s%d%s|", " ", liczba, " ");
    }
    printf("\n");
  }

  printf("\n");
  printf("==========================\n");
  printf("\n");

  liczba = 0;
  for (int i = 1; i <= 10; i++) {
    for (int j = 1; j <= 10; j++) {
      liczba = i * j;
      if ((liczba % 2) == 0) {
        printf("%*s|", 3, " - ");
        break; //<- pomija pętle wewnętrzną
      }
      (liczba > 9) ? printf("%3d|", liczba)
                   : printf("%s%d%s|", " ", liczba, " ");
    }
    printf("\n");
  }

  return 0;
}
