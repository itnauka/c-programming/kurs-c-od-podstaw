# Kurs C od podstaw

### [#6 - Instrukcja warunkowa ze znakiem zapytania ( ? : )](https://www.youtube.com/watch?v=z9BcUsq84F8&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=7)

<br />

```c
#include <stdio.h>


int main(void) {

  int a;

  printf("Podaj wiek: ");
  scanf("%d", &a);

  if ( a >= 18 ) printf("Osoba pełnoletnia, może pić wódkę\n");
  else printf("Osoba niepełnoletnia\n");

  return 0;
}
```

> **Podaj liczbę: 6**  
> **Osoba niepełnoletnia**

<br />

```c
#include <stdio.h>


int main(void) {

  int a;

  printf("Podaj wiek: ");
  scanf("%d", &a);

// (warunek) ? (gdy true) : (gdy false)

  ( a >= 18 ) ? printf("Osoba pełnoletnia, może pić wódkę\n") : printf("Osoba niepełnoletnia\n");

  return 0;
}
```

> **Podaj liczbę: 18**  
> **Osoba pełnoletnia, może pić wódkę**


