# Kurs C od podstaw

### [#12 - Preinkrementacja i postinkrementacja](https://www.youtube.com/watch?v=M6pp-qKmUqM&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=13)

<br />

```c
#include <complex.h>
#include <stdio.h>

int liczba1 = 7;

int main(void) {

  int liczba2 = liczba1;
  printf("%d\t%d\n", liczba1, liczba2); // 7  7

  liczba2 = liczba1++; //postinkrementacja
  printf("%d\t%d\n", liczba1, liczba2); // 8  7

  liczba2 = ++liczba1; //preinkrementacja
  printf("%d\t%d\n", liczba1, liczba2); // 9  9

  liczba2 = liczba1--; //postdekrementacja
  printf("%d\t%d\n", liczba1, liczba2); // 8  9

  liczba2 = --liczba1; //predekrementacja
  printf("%d\t%d\n", liczba1, liczba2); // 7  7


  return 0;
}
```

> **7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7**  
> **8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7**  
> **9&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9**  
> **8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9**  
> **7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7**



