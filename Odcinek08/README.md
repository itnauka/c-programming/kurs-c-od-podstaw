# Kurs C od podstaw

### [#8 - Pętla for, zadanie z tabliczką mnożenia](https://www.youtube.com/watch?v=qvyUBdWXCx8&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=10)

<br />

```c
#include <sthio.h>

int main(void) {

  int a, b = 0;//a - NIE zainicjalizowana

  printf("Podaj liczbę do której ma wypisywać: ");
  scanf("%d", &b);

  for(a = 0; a < b; a++){ //a - zainicjalizowana
    printf("%d\n", a);
  }

  return 0;
}
```

> **Podaj liczbę do której ma wypisywać: 6**  
> **0**  
> **1**  
> **2**  
> **3**  
> **4**  
> **5**

<br />

```c
#include <sthio.h>

int main(void) {

  int a = 0, b = 0;
  scanf("%d", &b);

  while (a < b)
    printf("%d\n", a);
    a++;
  }

  return 0;
}
```

> **6**  
> **0**  
> **1**  
> **2**  
> **3**  
> **4**  
> **5**

<br />

```c
#include <sthio.h>

int main(void) {

  int liczba = 0;
  for (int i = 1; i <= 10; i++) {
    for (int j = 1; j <= 10; j++) {
      liczba = i * j;
      printf("%3d|", liczba);
    }
    printf("\n");
  }

  return 0;
}
```

> **1| 2| 3| 4| 5| 6| 7| 8| 9| 10|**  
>  **2| 4| 6| 8| 10| 12| 14| 16| 18| 20|**  
>  **3| 6| 9| 12| 15| 18| 21| 24| 27| 30|**  
>  **4| 8| 12| 16| 20| 24| 28| 32| 36| 40|**  
>  **5| 10| 15| 20| 25| 30| 35| 40| 45| 50|**  
>  **6| 12| 18| 24| 30| 36| 42| 48| 54| 60|**  
>  **7| 14| 21| 28| 35| 42| 49| 56| 63| 70|**  
>  **8| 16| 24| 32| 40| 48| 56| 64| 72| 80|**  
>  **9| 18| 27| 36| 45| 54| 63| 72| 81| 90|**  
> **10| 20| 30| 40| 50| 60| 70| 80| 90|100|**

<table>
  <tr>
    <th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th>
  </tr>
  <tr>
    <th>2</th><th>4</th><th>6</th><th>8</th><th>10</th><th>12</th><th>14</th><th>16</th><th>18</th><th>20</th>
  </tr>
  <tr>
    <th>3</th><th>6</th><th>9</th><th>12</th><th>15</th><th>18</th><th>21</th><th>24</th><th>27</th><th>30</th>
  </tr>
  <tr>
    <th>4</th><th>8</th><th>12</th><th>16</th><th>20</th><th>24</th><th>28</th><th>32</th><th>36</th><th>40</th>
  </tr>
  <tr>
    <th>5</th><th>10</th><th>15</th><th>20</th><th>25</th><th>30</th><th>35</th><th>40</th><th>45</th><th>50</th>
  </tr>
  <tr>
    <th>6</th><th>12</th><th>18</th><th>24</th><th>30</th><th>36</th><th>42</th><th>48</th><th>54</th><th>60</th>
  </tr>
  <tr>
    <th>7</th><th>14</th><th>21</th><th>28</th><th>35</th><th>42</th><th>49</th><th>56</th><th>63</th><th>70</th>
  </tr>
  <tr>
    <th>8</th><th>16</th><th>24</th><th>32</th><th>40</th><th>48</th><th>56</th><th>64</th><th>72</th><th>80</th>
  </tr>
  <tr>
    <th>9</th><th>18</th><th>27</th><th>36</th><th>45</th><th>54</th><th>63</th><th>72</th><th>81</th><th>90</th>
  </tr>
  <tr>
    <th>10</th><th>20</th><th>30</th><th>40</th><th>50</th><th>60</th><th>70</th><th>80</th><th>90</th><th>100</th>
  </tr>
</table>  

<br />

```c
#include <sthio.h>

int main(void) {

  int liczba = 0;
  for (int i = 1; i <= 20; i++) {
    for (int j = 1; j <= 20; j++) {
      liczba = i * j;
      printf("%4d", liczba);
    }
    printf("\n");
  }

  return 0;
}
```

> **1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20**  
>  **2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38 40**  
>  **3 6 9 12 15 18 21 24 27 30 33 36 39 42 45 48 51 54 57 60**  
>  **4 8 12 16 20 24 28 32 36 40 44 48 52 56 60 64 68 72 76 80**  
>  **5 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100**  
>  **6 12 18 24 30 36 42 48 54 60 66 72 78 84 90 96 102 108 114 120**  
>  **7 14 21 28 35 42 49 56 63 70 77 84 91 98 105 112 119 126 133 140**  
>  **8 16 24 32 40 48 56 64 72 80 88 96 104 112 120 128 136 144 152 160**  
>  **9 18 27 36 45 54 63 72 81 90 99 108 117 126 135 144 153 162 171 180**  
>  **10 20 30 40 50 60 70 80 90 100 110 120 130 140 150 160 170 180 190 200**  
>  **11 22 33 44 55 66 77 88 99 110 121 132 143 154 165 176 187 198 209 220**  
>  **12 24 36 48 60 72 84 96 108 120 132 144 156 168 180 192 204 216 228 240**  
>  **13 26 39 52 65 78 91 104 117 130 143 156 169 182 195 208 221 234 247 260**  
>  **14 28 42 56 70 84 98 112 126 140 154 168 182 196 210 224 238 252 266 280**  
>  **15 30 45 60 75 90 105 120 135 150 165 180 195 210 225 240 255 270 285 300**  
>  **16 32 48 64 80 96 112 128 144 160 176 192 208 224 240 256 272 288 304 320**  
>  **17 34 51 68 85 102 119 136 153 170 187 204 221 238 255 272 289 306 323 340**  
>  **18 36 54 72 90 108 126 144 162 180 198 216 234 252 270 288 306 324 342 360**  
>  **19 38 57 76 95 114 133 152 171 190 209 228 247 266 285 304 323 342 361 380**  
>  **20 40 60 80 100 120 140 160 180 200 220 240 260 280 300 320 340 360 380 400**

<table>
  <tr>
    <th>1</th>
    <th>2</th>
    <th>3</th>
    <th>4</th>
    <th>5</th>
    <th>6</th>
    <th>7</th>
    <th>8</th>
    <th>9</th>
    <th>10</th>
    <th>11</th>
    <th>12</th>
    <th>13</th>
    <th>14</th>
    <th>15</th>
    <th>16</th>
    <th>17</th>
    <th>18</th>
    <th>19</th>
    <th>20</th>
  </tr>
  <tr>
    <th>2</th>
    <th>4</th>
    <th>6</th>
    <th>8</th>
    <th>10</th>
    <th>12</th>
    <th>14</th>
    <th>16</th>
    <th>18</th>
    <th>20</th>
    <th>22</th>
    <th>24</th>
    <th>26</th>
    <th>28</th>
    <th>30</th>
    <th>32</th>
    <th>34</th>
    <th>36</th>
    <th>38</th>
    <th>40</th>
  </tr>
  <tr>
    <th>3</th>
    <th>6</th>
    <th>9</th>
    <th>12</th>
    <th>15</th>
    <th>18</th>
    <th>21</th>
    <th>24</th>
    <th>27</th>
    <th>30</th>
    <th>33</th>
    <th>36</th>
    <th>39</th>
    <th>42</th>
    <th>45</th>
    <th>48</th>
    <th>51</th>
    <th>54</th>
    <th>57</th>
    <th>60</th>
  </tr>
  <tr>
    <th>4</th>
    <th>8</th>
    <th>12</th>
    <th>16</th>
    <th>20</th>
    <th>24</th>
    <th>28</th>
    <th>32</th>
    <th>36</th>
    <th>40</th>
    <th>44</th>
    <th>48</th>
    <th>52</th>
    <th>56</th>
    <th>60</th>
    <th>64</th>
    <th>68</th>
    <th>72</th>
    <th>76</th>
    <th>80</th>
  </tr>
  <tr>
    <th>5</th>
    <th>10</th>
    <th>15</th>
    <th>20</th>
    <th>25</th>
    <th>30</th>
    <th>35</th>
    <th>40</th>
    <th>45</th>
    <th>50</th>
    <th>55</th>
    <th>60</th>
    <th>65</th>
    <th>70</th>
    <th>75</th>
    <th>80</th>
    <th>85</th>
    <th>90</th>
    <th>95</th>
    <th>100</th>
  </tr>
  <tr>
    <th>6</th>
    <th>12</th>
    <th>18</th>
    <th>24</th>
    <th>30</th>
    <th>36</th>
    <th>42</th>
    <th>48</th>
    <th>54</th>
    <th>60</th>
    <th>66</th>
    <th>72</th>
    <th>78</th>
    <th>84</th>
    <th>90</th>
    <th>96</th>
    <th>102</th>
    <th>108</th>
    <th>114</th>
    <th>120</th>
  </tr>
  <tr>
    <th>7</th>
    <th>14</th>
    <th>21</th>
    <th>28</th>
    <th>35</th>
    <th>42</th>
    <th>49</th>
    <th>56</th>
    <th>63</th>
    <th>70</th>
    <th>77</th>
    <th>84</th>
    <th>91</th>
    <th>98</th>
    <th>105</th>
    <th>112</th>
    <th>119</th>
    <th>126</th>
    <th>133</th>
    <th>140</th>
  </tr>
  <tr>
    <th>8</th>
    <th>16</th>
    <th>24</th>
    <th>32</th>
    <th>40</th>
    <th>48</th>
    <th>56</th>
    <th>64</th>
    <th>72</th>
    <th>80</th>
    <th>88</th>
    <th>96</th>
    <th>104</th>
    <th>112</th>
    <th>120</th>
    <th>128</th>
    <th>136</th>
    <th>144</th>
    <th>152</th>
    <th>160</th>
  </tr>
  <tr>
    <th>9</th>
    <th>18</th>
    <th>27</th>
    <th>36</th>
    <th>45</th>
    <th>54</th>
    <th>63</th>
    <th>72</th>
    <th>81</th>
    <th>90</th>
    <th>99</th>
    <th>108</th>
    <th>117</th>
    <th>126</th>
    <th>135</th>
    <th>144</th>
    <th>153</th>
    <th>162</th>
    <th>171</th>
    <th>180</th>
  </tr>
  <tr>
    <th>10</th>
    <th>20</th>
    <th>30</th>
    <th>40</th>
    <th>50</th>
    <th>60</th>
    <th>70</th>
    <th>80</th>
    <th>90</th>
    <th>100</th>
    <th>110</th>
    <th>120</th>
    <th>130</th>
    <th>140</th>
    <th>150</th>
    <th>160</th>
    <th>170</th>
    <th>180</th>
    <th>190</th>
    <th>200</th>
  </tr>
  <tr>
    <th>11</th>
    <th>22</th>
    <th>33</th>
    <th>44</th>
    <th>55</th>
    <th>66</th>
    <th>77</th>
    <th>88</th>
    <th>99</th>
    <th>110</th>
    <th>121</th>
    <th>132</th>
    <th>143</th>
    <th>154</th>
    <th>165</th>
    <th>176</th>
    <th>187</th>
    <th>198</th>
    <th>209</th>
    <th>220</th>
  </tr>
  <tr>
    <th>12</th>
    <th>24</th>
    <th>36</th>
    <th>48</th>
    <th>60</th>
    <th>72</th>
    <th>84</th>
    <th>96</th>
    <th>108</th>
    <th>120</th>
    <th>132</th>
    <th>144</th>
    <th>156</th>
    <th>168</th>
    <th>180</th>
    <th>192</th>
    <th>204</th>
    <th>216</th>
    <th>228</th>
    <th>240</th>
  </tr>
  <tr>
    <th>13</th>
    <th>26</th>
    <th>39</th>
    <th>52</th>
    <th>65</th>
    <th>78</th>
    <th>91</th>
    <th>104</th>
    <th>117</th>
    <th>130</th>
    <th>143</th>
    <th>156</th>
    <th>169</th>
    <th>182</th>
    <th>195</th>
    <th>208</th>
    <th>221</th>
    <th>234</th>
    <th>247</th>
    <th>260</th>
  </tr>
  <tr>
    <th>14</th>
    <th>28</th>
    <th>42</th>
    <th>56</th>
    <th>70</th>
    <th>84</th>
    <th>98</th>
    <th>112</th>
    <th>126</th>
    <th>140</th>
    <th>154</th>
    <th>168</th>
    <th>182</th>
    <th>196</th>
    <th>210</th>
    <th>224</th>
    <th>238</th>
    <th>252</th>
    <th>266</th>
    <th>280</th>
  </tr>
  <tr>
    <th>15</th>
    <th>30</th>
    <th>45</th>
    <th>60</th>
    <th>75</th>
    <th>90</th>
    <th>105</th>
    <th>120</th>
    <th>135</th>
    <th>150</th>
    <th>165</th>
    <th>180</th>
    <th>195</th>
    <th>210</th>
    <th>225</th>
    <th>240</th>
    <th>255</th>
    <th>270</th>
    <th>285</th>
    <th>300</th>
  </tr>
  <tr>
    <th>16</th>
    <th>32</th>
    <th>48</th>
    <th>64</th>
    <th>80</th>
    <th>96</th>
    <th>112</th>
    <th>128</th>
    <th>144</th>
    <th>160</th>
    <th>176</th>
    <th>192</th>
    <th>208</th>
    <th>224</th>
    <th>240</th>
    <th>256</th>
    <th>272</th>
    <th>288</th>
    <th>304</th>
    <th>320</th>
  </tr>
  <tr>
    <th>17</th>
    <th>34</th>
    <th>51</th>
    <th>68</th>
    <th>85</th>
    <th>102</th>
    <th>119</th>
    <th>136</th>
    <th>153</th>
    <th>170</th>
    <th>187</th>
    <th>204</th>
    <th>221</th>
    <th>238</th>
    <th>255</th>
    <th>272</th>
    <th>289</th>
    <th>306</th>
    <th>323</th>
    <th>340</th>
  </tr>
  <tr>
    <th>18</th>
    <th>36</th>
    <th>54</th>
    <th>72</th>
    <th>90</th>
    <th>108</th>
    <th>126</th>
    <th>144</th>
    <th>162</th>
    <th>180</th>
    <th>198</th>
    <th>216</th>
    <th>234</th>
    <th>252</th>
    <th>270</th>
    <th>288</th>
    <th>306</th>
    <th>324</th>
    <th>342</th>
    <th>360</th>
  </tr>
  <tr>
    <th>19</th>
    <th>38</th>
    <th>57</th>
    <th>76</th>
    <th>95</th>
    <th>114</th>
    <th>133</th>
    <th>152</th>
    <th>171</th>
    <th>190</th>
    <th>209</th>
    <th>228</th>
    <th>247</th>
    <th>266</th>
    <th>285</th>
    <th>304</th>
    <th>323</th>
    <th>342</th>
    <th>361</th>
    <th>380</th>
  </tr>
  <tr>
    <th>20</th>
    <th>40</th>
    <th>60</th>
    <th>80</th>
    <th>100</th>
    <th>120</th>
    <th>140</th>
    <th>160</th>
    <th>180</th>
    <th>200</th>
    <th>220</th>
    <th>240</th>
    <th>260</th>
    <th>280</th>
    <th>300</th>
    <th>320</th>
    <th>340</th>
    <th>360</th>
    <th>380</th>
    <th>400</th>
  </tr>
</table>

