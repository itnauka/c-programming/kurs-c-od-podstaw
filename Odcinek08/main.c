// Kurs C od podstaw #8 - Pętla for, zadanie z tabliczką mnożenia
// https://www.youtube.com/watch?v=qvyUBdWXCx8

#include <stdio.h>

int main(void) {

  int a, b = 0;

  printf("Podaj liczbę do której ma wypisywać: ");
  scanf("%d", &b);

  for (a = 0; a < b; a++) {
    printf("%d\n", a);
  }

  printf("------------------\n");

// TABLICZKA MNOŻENIA
  int liczba = 0;
  for (int i = 1; i <= 10; i++) {
    for (int j = 1; j <= 10; j++) {
      liczba = i * j;
      printf("%3d|", liczba);
    }
    printf("\n");
  }

  return 0;
}
