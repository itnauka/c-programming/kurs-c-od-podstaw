// Kurs C od podstaw #16 - Funkcje
// https://www.youtube.com/watch?v=HqdNSSsr5pg

#include <stdio.h>

float srednia();
int dane[10] = { 5, 5, 9, 1, 10, 1, 4, 8, 5, 6 };

int main(void) { //wywoływanie
  float sredniax = srednia();
  printf("%f", sredniax);


  return 0;
}


// FUNKCJE
float srednia() {
  int suma = 0; 
  for (int i = 0; i < 10; i++) {
    suma += dane[i];
  }
  return (suma/10.0);
}

// for (int i = 0; i < 10; i++) suma += dane[i]; //wersja krótsza