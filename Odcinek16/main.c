// Kurs C od podstaw #16 - Funkcje
// https://www.youtube.com/watch?v=HqdNSSsr5pg

#include <stdio.h>

// zakres widoczności 'globalna'
void powiadomienie();
int dodaj(int a, int b);

int main(void) { // wywoływanie

  powiadomienie();

  int suma;
  suma = dodaj(5, 9);
  printf("%d\n", suma);

  return 0;
}

// FUNKCJE
void powiadomienie() { printf("Witaj w kursie programowania!!!\n"); }

int dodaj(int a, int b) {
  // int suma = a + b;
  // return suma;
  return a + b; // uproszczenie
}