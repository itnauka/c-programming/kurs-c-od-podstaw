# Kurs C od podstaw

### [#16 - Funkcje](https://www.youtube.com/watch?v=HqdNSSsr5pg&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=17)

```c
#include <stdio.h>

// zakres widoczności 'globalna'
void powiadomienie();

int main(void) {

  powiadomienie();// wywoływanie

  return 0;
}

// FUNKCJE
void powiadomienie(void) {//argument void w niektórych kompilatorach jest konieczny!!!
  printf("Witaj w kursie programowania!!!\n"); 
}
```

> **Witaj w kursie programowania!!!**

<br />

```c
#include <stdio.h>

int dodaj(int a, int b);

int main(void) {

  int suma;
  suma = dodaj(5, 9);
  printf("%d\n", suma);

  return 0;
}

// FUNKCJE
int dodaj(int a, int b) {
  // int suma = a + b;
  // return suma;
  return a + b; // uproszczenie
}
```

> **14**

<br />

```c
#include <stdio.h>

float srednia();
int dane[10] = { 5, 5, 9, 1, 10, 1, 4, 8, 5, 6 };

int main(void) {

  float sredniax = srednia();
  printf("%f", sredniax);

  return 0;
}


// FUNKCJE
float srednia() {
  int suma = 0; 
  for (int i = 0; i < 10; i++) suma += dane[i];
  }
  return (suma/10.0);
}
```

> **5.400000%**

<br />

```c
#include <stdio.h>

int dodaj(int *a, int *b);

int main(void) {
  
  int var1 = 5, var2 = 7;
  int sumax = dodaj(&var1, &var2);

  printf("%d\n", sumax);

  return 0;
}

// FUNKCJE
int dodaj(int *a, int *b) {
  int suma = *a + *b;
  return suma;
}
```

> **12**

<br />

```c
#include <stdio.h>

int dodaj(int *a, int *b);

int main(void) {
  
  int var1 = 5, var2 = 7;
  int sumax = dodaj(&var1, &var2);

  printf("%d %d\n", sumax, var1);

  return 0;
}

// FUNKCJE
int dodaj(int *a, int *b) {
  int suma = *a + *b;
  *a += 8; 
  return suma;
}
```

> **12 13**

<br />

```c
#include <stdio.h>

int dodaj(int a, int b);

int main(void) {
  
  int var1 = 5, var2 = 7;
  int sumax = dodaj(var1, var2);

  printf("%d %d\n", sumax, var1);

  return 0;
}

// FUNKCJE
int dodaj(int a, int b) {
  int suma = a + b;
  a += 8;//'a' jest lokalną zmienną (konieczne użycie wskaźnika '*a')
  return suma;
}
```

> **12 5**
