// Kurs C od podstaw #16 - Funkcje
// https://www.youtube.com/watch?v=HqdNSSsr5pg

#include <stdio.h>

int dodaj(int *a, int *b);

int main(void) {
  
  int var1 = 5, var2 = 7;
  int sumax = dodaj(&var1, &var2);

  printf("%d\n", sumax);
  // printf("%d %p\n", sumax, var1);


  return 0;
}

// FUNKCJE
int dodaj(int *a, int *b) {
  int suma = *a + *b;
  *a += 8;
  return suma;
}
//wskaźniki działają na oryginałach zmiennych
// zwykłe zmienne pracują na kopiach
// a bez gwiazdki pracujesz na wskźniku
// *a z gwizdką pracujesz na zmiennej na którą akurat ten wskaźnik wskazuje
// Funkcje mogą zwracać typ wskaźnikowy