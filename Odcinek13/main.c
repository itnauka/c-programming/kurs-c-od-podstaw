// Kurs C od podstaw #13 - Tablice
// https://www.youtube.com/watch?v=9g0ruHKztSg

#include <complex.h>
#include <stdio.h>

// TABLICE JEDNOWYMIAROWE

/*
int pomiar1;
int pomiar2;
int pomiar3;
int pomiar4;
 */

// int pomiar[4]; // deklaracja tablicy
// [4] rozmiar tablicy (4 elementowa)

// pomiar = 5; // inicjalizacja zwykłej zmiennej

/*
// inicjalizacja zmiennej w tablicy
  pomiar[0] = 5;
  pomiar[1] = 8;
  pomiar[2] = 7;
  pomiar[3] = 2;
*/

// nie trzeba deklarować długości elementów tablicy [] kiedy ją inicjalizujemy np. {5, 8, 7, 2}
int pomiar[] = {5, 8, 7, 2};
int tablica[10];

// TABLICE WIELOWYMIAROWE
// int tablicaW[10][10][10];
int multiTablica[3][3] = {
  { 1, 2, 3 },
  { 4, 5, 6 },
  { 7, 8, 9 }
};

int main(void) {

  printf("%d %d %d %d\n", pomiar[0], pomiar[1], pomiar[2], pomiar[3]);
  printf("%d\n", pomiar[4]); // = 0

  printf("===================\n");

  for (int i = 0; i < 4; i++) {
    printf("%d\n", pomiar[3 - i]);
  }

  printf("===================\n");

  for (int i = 0; i < 10; i++) {
    tablica[i] = i + 1;
  }
    for (int i = 0; i < 10; i++) {
    printf("%d\n", tablica[i]);
  }

  printf("===================\n");

// int multiTablica[3][3] = {
//   { 1, 2, 3 },
//   { 4, 5, 6 },
//   { 7, 8, 9 }
// };
  for (int i = 0; i < 3; i++) {
    printf("%d\n", multiTablica[1][i]);
  }
printf("--- --- ---\n");
  for (int i = 0; i < 3; i++) {
    printf("%d\n", multiTablica[i][2]);
  }
printf("--- --- ---\n");
  for (int i = 0; i < 3; i++) {
    printf("%d\n", multiTablica[i][i]);
  }
printf("--- --- ---\n");
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      printf("%d ", multiTablica[j][i]);

    }
    printf("\n");
  }



  return 0;
}
