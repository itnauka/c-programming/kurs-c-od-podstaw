# Kurs C od podstaw

### [#13 - Tablice](https://www.youtube.com/watch?v=9g0ruHKztSg&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=14)

<br />

#### TABLICE JEDNOWYMIAROWE

```c
// zwykłe zmienne, zastępujemy tablicami
int pomiar1;
int pomiar2;
int pomiar3;
int pomiar4;

// deklaracja tablicy 4 elementowej
int pomiar[4];

// inicjalizacja zwykłej zmiennej
pomiar = 5;


// inicjalizacja zmiennej w tablicy
pomiar[0] = 5;
pomiar[1] = 8;
pomiar[2] = 7;
pomiar[3] = 2;

// deklaracja i inicjalizacja tablicy
int pomiar[] = {5, 8, 7, 2};

```

<br />


```c
#include <complex.h>
#include <stdio.h>

int pomiar[4] = {5, 8, 7, 2};

int main(void) {

  printf("%d %d %d %d\n", pomiar[0], pomiar[1], pomiar[2], pomiar[3]);
  printf("%d\n", pomiar[4]); // = 0

  return 0;
}
```

> **5 8 7 2**  
> **0**
>> Warrning: 0 jest poza zakresem tablicy

<br />

```c
#include <complex.h>
#include <stdio.h>

int pomiar[] = {5, 8, 7, 2};

int main(void) {

  for (int i = 0; i < 4; i++) {
    printf("%d\n", pomiar[i]);
  }

  return 0;
}
```

> **5**  
> **8**  
> **7**  
> **2**  

<br />

```c
#include <complex.h>
#include <stdio.h>

int pomiar[] = {5, 8, 7, 2};

int main(void) {

  for (int i = 0; i < 4; i++) {
    printf("%d\n", pomiar[3 - i]);
  }

  return 0;
}
```

> **2**  
> **7**  
> **8**  
> **5**  

<br />

```c
#include <complex.h>
#include <stdio.h>

int pomiar[12];

int main(void) {

  for (int i = 0; i < 12; i++) pomiar[i] = i + 1;
  for (int i = 0; i < 12; i++) {
    printf("%d\n", pomiar[i]);
  }

  return 0;
}
```

> **1**  
> **2**  
> **3**  
> **4**  
> **5**  
> **6**  
> **7**  
> **8**  
> **9**  
> **10**  
> **11**  
> **12**  

<br />

#### TABLICE WIELOWYMIAROWE

```c
// tablica trójwymiarowa
int tablicaW[10][10][10];

// tablica dwuwymiarowa
int multiTablica[3][3] = {
  { 1, 2, 3 },
  { 4, 5, 6 },
  { 7, 8, 9 }
};

```

<br />

```c
#include <complex.h>
#include <stdio.h>

int pomiar[3][3] = {
  { 1, 2, 3 },
  { 4, 5, 6 },
  { 7, 8, 9 }
};

int main(void) {

  for (int i = 0; i < 12; i++) printf("%d\n", pomiar[i][0]);

  return 0;
}
```

> **1**  
> **4**  
> **7**  

<br />

```c
#include <complex.h>
#include <stdio.h>

int pomiar[3][3] = {
  { 1, 2, 3 },
  { 4, 5, 6 },
  { 7, 8, 9 }
};

int main(void) {

  for (int i = 0; i < 12; i++) printf("%d\n", pomiar[i][2]);

  return 0;
}
```

> **3**  
> **6**  
> **9**  

<br />

```c
#include <complex.h>
#include <stdio.h>

int pomiar[3][3] = {
  { 1, 2, 3 },
  { 4, 5, 6 },
  { 7, 8, 9 }
};

int main(void) {

  for (int i = 0; i < 12; i++) printf("%d\n", pomiar[0][i]);

  return 0;
}
```

> **1**  
> **2**  
> **3**  

<br />

```c
#include <complex.h>
#include <stdio.h>

int pomiar[3][3] = {
  { 1, 2, 3 },
  { 4, 5, 6 },
  { 7, 8, 9 }
};

int main(void) {

  for (int i = 0; i < 12; i++) printf("%d\n", pomiar[0][i]);

  return 0;
}
```

> **4**  
> **5**  
> **6**  

<br />

```c
#include <complex.h>
#include <stdio.h>

int pomiar[3][3] = {
  { 1, 2, 3 },
  { 4, 5, 6 },
  { 7, 8, 9 }
};

int main(void) {

  for (int i = 0; i < 12; i++) printf("%d\n", pomiar[i][i]);

  return 0;
}
```

> **1**  
> **5**  
> **9**  

<br />

```c
#include <complex.h>
#include <stdio.h>

int pomiar[3][3] = {
  { 1, 2, 3 },
  { 4, 5, 6 },
  { 7, 8, 9 }
};

int main(void) {

  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      printf("%d ", multiTablica[j][i]);
    }
    printf("\n");
  }
}
```

> **1 4 7**  
> **2 5 8**  
> **3 6 9**  