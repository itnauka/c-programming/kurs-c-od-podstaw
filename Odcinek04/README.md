# Kurs C od podstaw

### [#4 - Operatory logiczne i bitowe](https://www.youtube.com/watch?v=3QiUMjoH3E0&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=5)

<br />

#### 1) Operacje bitowe

| "&" | a   | b   |
| --- | --- | --- |
| 0   | 0   | 0   |
| 1   | 1   | 1   |
| 0   | 0   | 1   |
| 0   | 1   | 0   |

| "\|" | a   | b   |
| ---- | --- | --- |
| 0    | 0   | 0   |
| 1    | 1   | 1   |
| 1    | 0   | 1   |
| 1    | 1   | 0   |

| "^" | a   | b   |
| --- | --- | --- |
| 0   | 0   | 0   |
| 1   | 1   | 1   |
| 1   | 0   | 1   |
| 1   | 1   | 0   |

| "~" | a   |
| --- | --- |
| 0   | 1   |
| 1   | 0   |
| 0   | 0   |
| 0   | 1   |

<br />

##### 1.1) AND

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 11;

  wynik = a & b;
  printf("%u\n", wynik);

  return 0;
}
```

> **1**

<br />

##### 1.2) OR

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 11;

  wynik = a | b;
  printf("%u\n", wynik);

  return 0;
}
```

> **15**

<br />

##### 1.3) XOR

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 11;

  wynik = a ^ b;
  printf("%u\n", wynik);

  return 0;
}
```

> **14**

<br />

| Variable | Binary (16-bit)     |
| -------- | ------------------- |
| a = 5    | 0000 0000 0000 0101 |
| a = 11   | 0000 0000 0000 1011 |

| Operation   | Result (Binary)     | Result (Decimal) |
| ----------- | ------------------- | ---------------- |
| a & b (AND) | 0000 0000 0000 0001 | 1                |
| a \| b (OR) | 0000 0000 0000 1111 | 15               |
| a ^ b (XOR) | 0000 0000 0000 1110 | 14               |

---

<br />

##### 1.4) AND

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 25;
  b = 37;

  wynik = a & b;
  printf("%u\n", wynik);

  return 0;
}
```

> **1**

<br />

##### 1.5) OR

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 25;
  b = 37;

  wynik = a | b;
  printf("%u\n", wynik);

  return 0;
}
```

> **61**

<br />

##### 1.6) XOR

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 25;
  b = 37;

  wynik = a ^ b;
  printf("%u\n", wynik);

  return 0;
}
```

> **60**

<br />

| Variable | Binary (16-bit)     |
| -------- | ------------------- |
| a = 25   | 0000 0000 0001 1001 |
| b = 37   | 0000 0000 0010 0101 |

| Operation   | Result (Binary)     | Result (Decimal) |
| ----------- | ------------------- | ---------------- |
| a & b (AND) | 0000 0000 0000 0001 | 1                |
| a \| b (OR) | 0000 0000 0011 1101 | 61               |
| a ^ b (XOR) | 0000 0000 0011 1100 | 60               |

---

<br />

##### 1.7) NOT AND

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 25;
  b = 37;

  wynik = ~(a & b);
  printf("%u\n", wynik);

  return 0;
}
```

> **65534**

<br />

##### 1.8) NOT OR

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 25;
  b = 37;

  wynik = ~(a | b);
  printf("%u\n", wynik);

  return 0;
}
```

> **65474**

<br />

##### 1.9) NOT XOR

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 25;
  b = 37;

  wynik = ~(a ^ b);
  printf("%u\n", wynik);

  return 0;
}
```

> **65475**

<br />

| Operation       | Result (Binary)     | Result (Decimal) |
| --------------- | ------------------- | ---------------- |
| a & b (NOT AND) | 1111 1111 1111 1110 | 65534            |
| a \| b (NOT OR) | 1111 1111 1100 0010 | 65474            |
| a ^ b (NOT XOR) | 1111 1111 1100 0011 | 65475            |

<br />

#### 2) Operacje logiczne

<br />

| A   | B   | A && B |
| --- | --- | ------ |
| 1   | 1   | 1      |
| 1   | 0   | 0      |
| 0   | 1   | 0      |
| 0   | 0   | 0      |

| A   | B   | A \|\| B |
| --- | --- | -------- |
| 1   | 1   | 1        |
| 1   | 0   | 1        |
| 0   | 1   | 1        |
| 0   | 0   | 0        |

| A   | !A  |
| --- | --- |
| 1   | 0   |
| 0   | 1   |

| "&" | a   | b   |
| --- | --- | --- |
| 0   | 0   | 0   |
| 1   | 1   | 1   |
| 0   | 0   | 1   |
| 0   | 1   | 0   |

<br />

##### 2.1) operator AND (&&)

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 25;//[false = 0], [true > 0]
  b = 37;

  wynik = a && b;
  printf("%u\n", wynik);

  return 0;
}
```

> **1**
>
> > prawda czyli **true**

<br />

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 0;//false
  b = 37;

  wynik = a && b;
  printf("%u\n", wynik);

  return 0;
}
```

> **0**
>
> > fałsz czyli **false**

<br />

##### 2.2) operator OR (||)

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 25;
  b = 37;

  wynik = a || b;
  printf("%u\n", wynik);

  return 0;
}
```

> **1**
>
> > prawda czyli **true**

<br />

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 0;
  b = 37;

  wynik = a || b;
  printf("%u\n", wynik);

  return 0;
}
```

> **1**
>
> > prawda czyli **true**

<br />

##### 2.3) operator równa się (==)

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 0;
  b = 0;

  wynik = a == b;
  printf("%u\n", wynik);

  return 0;
}
```

> **1**
>
> > prawda czyli **true**

<br />

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 0;

  wynik = a == b;
  printf("%u\n", wynik);

  return 0;
}
```

> **0**
>
> > fałsz czyli **false**

<br />

##### 2.4) operator różne (!=)

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 0;

  wynik = a != b;
  printf("%u\n", wynik);

  return 0;
}
```

> **1**
>
> > prawda czyli **true**

<br />

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 5;

  wynik = a != b;
  printf("%u\n", wynik);

  return 0;
}
```

> **0**
>
> > fałsz czyli **false**

<br />

##### 2.5) operator większy (>)

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 5;

  wynik = a > b;
  printf("%u\n", wynik);

  return 0;
}
```

> **0**
>
> > fałsz czyli **false**

<br />

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 4;

  wynik = a > b;
  printf("%u\n", wynik);

  return 0;
}
```

> **1**
>
> > prawda czyli **true**

<br />

##### 2.6) operator mniejszy (<)

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 4;

  wynik = a < b;
  printf("%u\n", wynik);

  return 0;
}
```

> **0**
>
> > fałsz czyli **false**

<br />

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 4;
  b = 7;

  wynik = a < b;
  printf("%u\n", wynik);

  return 0;
}
```

> **1**
>
> > prawda czyli **true**

<br />

##### 2.7) operator mniejszy bądź równy (<=)

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 4;

  wynik = a <= b;
  printf("%u\n", wynik);

  return 0;
}
```

> **0**
>
> > fałsz czyli **false**

<br />

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 7;
  b = 7;

  wynik = a < b;
  printf("%u\n", wynik);

  return 0;
}
```

> **1**
>
> > prawda czyli **true**

<br />

##### 2.8) operator większy bądź równy (>=)

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 7;
  b = 7;

  wynik = a >= b;
  printf("%u\n", wynik);

  return 0;
}
```

> **1**
>
> > prawda czyli **true**

<br />

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 10;
  b = 7;

  wynik = a >= b;
  printf("%u\n", wynik);

  return 0;
}
```

> **1**
>
> > prawda czyli **true**

<br />

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 2;
  b = 7;

  wynik = a >= b;
  printf("%u\n", wynik);

  return 0;
}
```

> **0**
>
> > fałsz czyli **false**

<br />

#### 3) Przesunięcia bitowe

<br />

##### 3.1) przesunięcia w lewo

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 10;

  wynik = a << 1;
  printf("%u\n", wynik);

  return 0;
}
```

> **10**

<br />

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 10;

  wynik = a << 2;
  printf("%u\n", wynik);

  return 0;
}
```

> **20**

<br />

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 10;

  wynik = a << 3;
  printf("%u\n", wynik);

  return 0;
}
```

> **40**

<br />

##### 3.2) przesunięcia w prawo

<br />

```c
#include <stdio.h>

int main(void) {

  unsigned short int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 10;

  wynik = a >> 1;
  printf("%u\n", wynik);

  return 0;
}
```

> **2**
