// Kurs C od podstaw #4 - Operatory logiczne i bitowe
// https://www.youtube.com/watch?v=3QiUMjoH3E0
// https://pl.wikibooks.org/wiki/C/Operatory
// https://slideplayer.pl/slide/2905575/10/images/5/BRAMKI+LOGICZNE.jpg
// https://www.programiz.com/c-programming/c-operators

#include <stdio.h>
/*
a = 5     0000 0000 0000 0101
b = 11    0000 0000 0000 1011

a & b     0000 0000 0000 0001   (1) AND
a | b     0000 0000 0000 1111   (15) OR
a ^ b     0000 0000 0000 1110   (14) XOR

---------------------------------------

a = 25    0000 0000 0001 1001
b = 37    0000 0000 0010 0101

a & b     0000 0000 0000 0001   (1)
a | b     0000 0000 0011 1101   (61)
a ^ b     0000 0000 0011 1100   (60)
*/

int main(void) {

  printf("========= - BITOWE - =========\n");
  printf("\n");
// OPERACJE BITOWE
  unsigned short int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 11;

  wynik = a & b;
  printf("%u\n", wynik); // = 1

  wynik = a | b;
  printf("%u\n", wynik); // = 15

  wynik = a ^ b;
  printf("%u\n", wynik); // = 14

  printf("-------------------------\n");

  a = 25;
  b = 37;

  wynik = a & b;
  printf("%u\n", wynik); // = 1

  wynik = a | b;
  printf("%u\n", wynik); // = 61

  wynik = a ^ b;
  printf("%u\n", wynik); // = 60

  printf("-------------------------\n");

  a = 25;
  b = 37;

  // a & b     0000 0000 0000 0001   (1)
  // a | b     0000 0000 0011 1101   (61)
  // a ^ b     0000 0000 0011 1100   (60)
  // ------ negacja NOT ------ //
  // a & b     1111 1111 1111 1110   (65534)
  // a | b     1111 1111 1100 0010   (35474)
  // a ^ b     1111 1111 1100 0011   (65475)

  wynik = ~(a & b);
  printf("%u\n", wynik); // = 65534

  wynik = ~(a | b);
  printf("%u\n", wynik); // = 35474

  wynik = ~(a ^ b);
  printf("%u\n", wynik); // = 65475

  printf("\n");
  printf("========= - LOGICZNE - =========\n");
  printf("\n");

// OPERACJE LOGICZNE
  // liczba > 0 = true, 0 = false
  wynik = a && b;
  printf("%u\n", wynik); // = 1 true

  wynik = a || b;
  printf("%u\n", wynik); // = 1 true

  wynik = (a == b);
  printf("%u\n", wynik); // = 0 false

  wynik = (a != b);
  printf("%u\n", wynik); // = 1 true

  wynik = (a > b);
  printf("%u\n", wynik); // = 0 false

  wynik = (a < b);
  printf("%u\n", wynik); // = 1 true

  wynik = (a <= b);
  printf("%u\n", wynik); // = 1 true

  wynik = (a >= b);
  printf("%u\n", wynik); // = 0 false


  printf("\n");
  printf("========= - PRZESUNIĘCIA BITOWE - =========\n");
  printf("\n");

// OPRZESUNIĘCIA BITOWE
//   a   | a<<1 | a<<2 | a>>1 | a>>2
// ------+------+------+------+------
//  0001 | 0010 | 0100 | 0000 | 0000
//  0011 | 0110 | 1100 | 0001 | 0000
//  0101 | 1010 | 0100 | 0010 | 0001
//  1000 | 0000 | 0000 | 0100 | 0010
//  1111 | 1110 | 1100 | 0111 | 0011
//  1001 | 0010 | 0100 | 0100 | 0010

  a = 5;
  b = 10;

  wynik = a << 1; //mnoży x 2, a<<1 *2 = 10
  printf("%u\n", wynik); // = 5

  wynik = a >> 1;
  printf("%u\n", wynik); // = 2


  return 0;
}
