<div align="center">
  <a href="https://www.youtube.com/@KoW">
    <img src="https://i.ytimg.com/vi/YaRPGzOy_WY/maxresdefault.jpg" width="600px" alt="Kanał o Wszystkim" />
  </a>
<h1>Damian Orzechowski</h1>
</div>

## Informacje o Kursie

- **Nazwa Kursu:** Kurs C od podstaw
- **Autor/Kreator:** Damian Orzechowski
- **Opis Kursu:** Kurs języka 'C' dla początkujących obejmujący podstawowe zagadnienia związane z programowaniem w tej technologii.
- **Link do Kursu:** [Kurs C od podstaw na YouTube](https://www.youtube.com/watch?v=YaRPGzOy_WY&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP)

## Struktura Kursu

1. [#0 - Wstęp](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek00?ref_type=heads)
2. [#1 - Typy danych, zmienne i stałe](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek01?ref_type=heads)
3. [#2 - Rzutowanie zmiennych](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek02?ref_type=heads)
4. [#3 - Operacje matematyczne na zmiennych](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek03?ref_type=heads)
5. [#4 - Operatory logiczne i bitowe](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek04?ref_type=heads)
6. [#5 - Instrukcja warunkowa if else](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek05?ref_type=heads)
7. [#6 - Instrukcja warunkowa ze znakiem zapytania ( ? : )](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek06?ref_type=heads)
8. [#7 - Pętla while oraz do...while](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek07?ref_type=heads)
9. [#8 - Pętla for, zadanie z tabliczką mnożenia](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek08?ref_type=heads)
10. [#9 - Instrukcja continue oraz break](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek09?ref_type=heads)
11. [#10 - Instrukcja switch...case](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek10?ref_type=heads)
12. [#11 - Zakres widoczności zmiennych](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek11?ref_type=heads)
13. [#12 - Preinkrementacja i postinkrementacja](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek12?ref_type=heads)
14. [#13 - Tablice](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek13?ref_type=heads)
15. [#14 - Wskaźniki](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek14?ref_type=heads)
16. [#15 - Znaki i ciągi znaków (string)](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek15?ref_type=heads)
17. [#16 - Funkcje](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek16?ref_type=heads)
18. [#17 - Struktury](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek17?ref_type=heads)
19. [#18 - Preprocesor](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek18?ref_type=heads)
20. [#19 - Kalkulator, stringi, geometria, suma ciągu](https://gitlab.com/itnauka/c-programming/kurs-c-od-podstaw/-/tree/main/Odcinek19?ref_type=heads)

## Wymagania

- [www.replit.com](https://replit.com/)

## Instrukcje dla Uczestników

Kurs jest przeznaczony głównie dla osób początkujących, jest łatwy do zrozumienia i skupia się na podstawowych zagadnieniach związanych z językiem C.

## Dodatkowe Materiały

Dodatkowe materiały znajdują się na osobnych stronach:

- [Typy danych](https://www.geeksforgeeks.org/data-types-in-c/ "geeksforgeeks.org")
- [Operatory programowania C](https://www.programiz.com/c-programming/c-operators "programiz.com")
- [Bramki logiczne](https://slideplayer.pl/slide/2905575/10/images/5/BRAMKI+LOGICZNE.jpg "slideplayer.pl")
- [Operatory](https://pl.wikibooks.org/wiki/C/Operatory "wikibooks.org")
- [Wskaźniki](https://pl.wikibooks.org/wiki/C/Wska%C5%BAniki "wikibooks.org")
- [Typy złożone](https://pl.wikibooks.org/wiki/C/Typy_z%C5%82o%C5%BCone#Unie "wikibooks.org")
- [Preprocesor](https://pl.wikibooks.org/wiki/C/Preprocesor "wikibooks.org")

## Licencja

Cały kurs podstawowy jest udostępniony na platformie YouTube i można swobodnie korzystać z materiałów.

## Kontakt

- E-mail: [jackoski@gmail.com](mailto:jackoski@gmail.com)
