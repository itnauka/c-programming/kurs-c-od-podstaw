# Kurs C od podstaw

### [#1 - Typy danych, zmienne i stałe](https://www.youtube.com/watch?v=tAuFdQorVEs&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=3)

<br />

| Data Type              | Size (bytes) | Range                           | Format Specifier |
| ---------------------- | ------------ | ------------------------------- | ---------------- |
| short int              | 2            | -32,768 to 32,767               | %hd              |
| unsigned short int     | 2            | 0 to 65,535                     | %hu              |
| unsigned int           | 4            | 0 to 4,294,967,295              | %u               |
| int                    | 4            | -2,147,483,648 to 2,147,483,647 | %d               |
| long int               | 4            | -2,147,483,648 to 2,147,483,647 | %ld              |
| unsigned long int      | 4            | 0 to 4,294,967,295              | %lu              |
| long long int          | 8            | -(2^63) to (2^63)-1             | %lld             |
| unsigned long long int | 8            | 0 to 18,446,744,073,709,551,615 | %llu             |
| signed char            | 1            | -128 to 127                     | %c               |
| unsigned char          | 1            | 0 to 255                        | %c               |
| float                  | 4            | 1.2E-38 to 3.4E+38              | %f               |
| double                 | 8            | 1.7E-308 to 1.7E+308            | %lf              |
| long double            | 16           | 3.4E-4932 to 1.1E+4932          | %Lf              |

<br />

```c
#include <stdio.h>

int main(void) {

  int x = 5, y = 7;
  const float PI = 3.14159; // stała

  printf("Wartość zmiennej x wynosi %d\n", x);
  printf("Wartość zmiennej y wynosi %d\n", y);
  printf("Wartość zmiennej PI wynosi %f\n", PI);

  return 0;
}
```

>**Wartość zmiennej x wynosi 5**
>**Wartość zmiennej y wynosi 7**
>**Wartość zmiennej PI wynosi 3.141590**

<br />

```c
#include <stdio.h>

int main(void) {

  int x = 5, y = 7;
  const float PI = 3.14159; // stała

  x = 9;
  printf("Wartość zmiennej PI wynosi %.4f (4 msc po przecinku)\n", PI);
  printf("Wartośći zmiennych x = %d, y = %d, PI = %f\n", x, y, PI);

  return 0;
}
```

>**Wartość zmiennej PI wynosi 3.1416 (4 msc po przecinku)**
>**Wartośći zmiennych x = 9, y = 7, PI = 3.141590**
