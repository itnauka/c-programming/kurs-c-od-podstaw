// Kurs C od podstaw #1 - Typy danych, zmienne i stałe
// https://www.youtube.com/watch?v=tAuFdQorVEs
// https://www.geeksforgeeks.org/data-types-in-c/

#include <stdio.h>

int main(void) {



  int x = 5, y = 7;
  const float PI = 3.14159; // stała

  printf("Wartość zmiennej x wynosi %d\n", x);
  printf("Wartość zmiennej y wynosi %d\n", y);
  printf("Wartość zmiennej PI wynosi %f\n", PI);

  x = 9;
  printf("Wartość zmiennej PI wynosi %.4f (4 msc po przecinku)\n", PI);
  printf("Wartośći zmiennych x = %d, y = %d, PI = %f\n", x, y, PI);
  return 0;
}

// -------------------------------------------------------------------------

  // int myNum = 15;

  // int myNum2;
  // myNum2 = 15;

  // int myNum3 = 15;
  // myNum3 = 10;

  // float myFloat = 5.99; // liczba zmiennoprzecinkowa
  // char myLetter = 'D';  // znak

  // int x = 5;
  // int y = 6;
  // int sum = x + y; // dodaje zmienne do sumy

  // // deklaracja wielu zmiennych
  // int x = 5, y = 6, z = 50;