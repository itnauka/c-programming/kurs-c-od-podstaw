# Kurs C od podstaw

### [#0 - Wstęp](https://www.youtube.com/watch?v=YaRPGzOy_WY&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=1)

```c
#include <stdio.h>

int main(void)
{
  printf("Hello World\n");
  printf("Hello World\n ciąg dalszy");

  return 0;
}
```

>**Hello World
>Hello World
>&nbsp;ciąg dalszy%**

