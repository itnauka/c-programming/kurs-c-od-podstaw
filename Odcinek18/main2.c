#include <stdio.h>
#define wypisz(x) printf("%s=%i\n", #x, x)
#define abc(x) int x##_zmienna

int main() {
  int liczba = 1;
  char a = 5;
  wypisz(liczba);
  wypisz(a);

  abc(nasza) = 3;
  // Robi dwie rzeczy:
  //  1.	Wstawia słowo „nasza” przed słowem „ _zmienna”.
  //  Dzięki temu zadeklarujemy zmienną o nazwie "nasza_zmienna".
  //  2.	Inicjalizuje „nasza_zmienna” wartością "2".
    wypisz(nasza_zmienna);

  return 0;
}