# Kurs C od podstaw

### [#18 - Preprocesor](https://www.youtube.com/watch?v=0R7oJW_-yZE&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=19)

```c
#include <stdio.h>
#include "funkcje.h"

#define NAPIS "Hello World\n"
#define pf(a) printf(a)

int main(void) {

  printf(NAPIS);
  komunikat();

  return 0;
}
```

> **Hello World**  
> **Jestem funkcją w oddzielnym pliku :-\)**  

<br />

```c
#include <stdio.h>
#include "funkcje.h"

#define NAPIS "Hello World\n"
#define pf(a) printf(a)

int main(void) {

  pf(NAPIS);
  komunikat();

  return 0;
}
```

> **Hello World**  
> **Jestem funkcją w oddzielnym pliku :-\)**  

<br />

```c
#include <stdio.h>
#include "funkcje.h"

#define NAPIS "Hello World\n"
#define pf(a) printf(a)
#define PI 3.14159

int main(void) {

  pf(NAPIS);
  printf("%f\n", PI);
  komunikat();

  return 0;
}
```

> **Hello World**  
> **3.14159**  
> **Jestem funkcją w oddzielnym pliku :-\)**  

<br />

```c
#include <stdio.h>
#include "funkcje.h"

#define NAPIS "Hello World\n"
#define pf(a) printf(a)
#define PI 3.14159
#define dodaj(a, b) (a + b)//MAKRO - krótka funkcja

int main(void) {

  pf(NAPIS);
  printf("%f\n", PI);
  printf("%d\n", dodaj(5, 7));
  komunikat();

  return 0;
}
```

> **Hello World**  
> **3.14159**  
> **12**  
> **Jestem funkcją w oddzielnym pliku :-\)**  

<br />

```c
#include <stdio.h>
#include "funkcje.h"

#define NAPIS "Hello World\n"
#define pf(a) printf(a)
#define PI 3.14159
#define dodaj(a, b) (a + b)
#undef PI// <- usunięta 

int main(void) {

  pf(NAPIS);
  printf("%f\n", PI);//<- wywoływana
  printf("%d\n", dodaj(5, 7));
  komunikat();

  return 0;
}
```

> **main.c: In function ‘main’:**  
> **main.c:37:18: error: ‘PI’ undeclared (first use in this function)**  
> &nbsp;&nbsp;&nbsp;**37 |   printf("%f\n", PI);**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|** &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **^~**  
> **main.c:37:18: note: each undeclared identifier is reported only once for each function it appears in**  

<br />

```c
#include <stdio.h>
#include "funkcje.h"

#define NAPIS "Hello World\n"
#define pf(a) printf(a)
#define PI 3.14159
#define dodaj(a, b) (a + b)

#define A 5

#if A < 10
#warning "Zła definicja A!!!"
#endif

int main(void) {

  pf(NAPIS);
  printf("%f\n", PI);
  printf("%d\n", dodaj(5, 7));
  komunikat();

  return 0;
}
```

> **main.c:17:2: warning: #warning "Zła definicja A!!!" [-Wcpp]**  
> &nbsp;&nbsp;&nbsp;**17 | #warning "Zła definicja A !!!"**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|** &nbsp;&nbsp;**^~~~~~~**  

<br />

```c
#include <stdio.h>
#include "funkcje.h"

#define A 5

#ifdef A
#warning "Zdefiniowano A"
#endif

int main(void) {

  pf(NAPIS);
  printf("%f\n", PI);
  printf("%d\n", dodaj(5, 7));
  komunikat();

  return 0;
}
```

> **main.c:27:2: warning: #warning "Zdefiniowano A" [-Wcpp]**  
> &nbsp;&nbsp;&nbsp;**27 | #warning "Zdefiniowano A"**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **|** &nbsp; **^~~~~~~**  

<br />

```c
#include <stdio.h>
#include "funkcje.h"

#ifndef A
#warning "NIE zdefiniowano A"
#endif

int main(void) {

  pf(NAPIS);
  printf("%f\n", PI);
  printf("%d\n", dodaj(5, 7));
  komunikat();

  return 0;
}
```

> **main.c:27:2: warning: #warning "Zdefiniowano A" [-Wcpp]**  
> &nbsp;&nbsp;&nbsp;**27 | #warning "NIE zdefiniowano A"**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **|** &nbsp; **^~~~~~~**  

<br />

```c
#include <stdio.h>
#include "funkcje.h"

#ifndef A
#error "NIE zdefiniowano A"
#endif

int main(void) {

  pf(NAPIS);
  printf("%f\n", PI);
  printf("%d\n", dodaj(5, 7));
  komunikat();

  return 0;
}
```

> **main.c:28:2: error: #error "NIE Zdefiniowano A"**  
> &nbsp;&nbsp; **28 | #error "NIE Zdefiniowano A"**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **|** &nbsp; **^~~~~**  

<br />

```c
#include <stdio.h>
#include "funkcje.h"

#ifndef A
#error "NIE zdefiniowano A"
#endif

int main(void) {

  pf(NAPIS);
  printf("%f\n", PI);
  printf("%d\n", dodaj(5, 7));
  #line 38 //zeruje numery linii (38)
  komunikat();

  return 0;
}
```

> **main.c: In function ‘main’:**  
>**main.c:38:14: error: expected ‘;’ before ‘return’**  
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**38 | }**  
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|** &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **^**  
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|** &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **;**  
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**39** **|**  

<br />

#### znak "#" w makrach

```c
#include <stdio.h>
#define wypisz(x) printf("%s=%i\n", #x, x)

int main() {
  int liczba = 1;
  char a = 5;
  wypisz(liczba);
  wypisz(a);

  return 0;
}
```

> **liczba=1**  
> **a=5**  
>> Czyli wypisz(a) jest rozwijane w printf("%s=%i\n", =="a"==, a).

<br />

#### znak "##" w makrach

```c
#include <stdio.h>
#define wypisz(x) printf("%s=%i\n", #x, x)
#define abc(x) int x##_zmienna

int main() {

  abc(nasza) = 3;
              // Robi dwie rzeczy:
              //  1.	Wstawia słowo „nasza” przed słowem „ _zmienna”.
              //  Dzięki temu zadeklarujemy zmienną o nazwie "nasza_zmienna".
              //  2.	Inicjalizuje „nasza_zmienna” wartością "2".
    wypisz(nasza_zmienna);

  return 0;
}
```

> **nasza_zmienna=3**  



