// Kurs C od podstaw #18 - Preprocesor
// https://www.youtube.com/watch?v=0R7oJW_-yZE
// https://pl.wikibooks.org/wiki/C/Preprocesor

#include "funkcje.h"
#include <stdio.h>

#define NAPIS "Hello World\n"
#define pf(a) printf(a)
#define PI 3.14159
#define dodaj(a, b) (a + b)

// #undef PI

// #define A 5

// #if A < 10
// #warning "Zła definicja A!!!"
// #endif

// #if A < 10
// #error "Zła definicja A!!!"
// #endif

// #ifndef A //ifdef - zdefiniowano 'A'
// #error "NIE Zdefiniowano A"
// #endif

int main(void) {

  // pf(NAPIS);
  printf("%f\n", PI);
  printf("%d\n", dodaj(5, 7));
   
  komunikat()

  return 0;
}

