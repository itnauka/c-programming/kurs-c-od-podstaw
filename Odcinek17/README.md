# Kurs C od podstaw

### [#17 - Struktury](https://www.youtube.com/watch?v=mcAMD3DfdbI&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=18)

```c
#include <stdio.h>

typedef char* String;

struct car {
  String model;
  String VIN;
  unsigned int ilosc_miejsc;
  unsigned int masa;
  unsigned short rok_prod;
};

int main(void) {

  struct car samochod1, samochod2;

  samochod1.model = "Skoda Fabia II";
  samochod1.rok_prod = 2012;
  samochod1.ilosc_miejsc = 5;
  samochod1.masa = 980;

  samochod2.model = "Audi A4 B5";
  samochod2.rok_prod = 1999;
  samochod2.ilosc_miejsc = 5;
  samochod2.masa = 1100;

  printf("Model: %s\nRok produkcji: %d\nIlość miejsc: %d\nMasa: %d\n\n", 
      samochod1.model, samochod1.rok_prod, samochod1.ilosc_miejsc, samochod1.masa);
      
  printf("Model: %s\nRok produkcji: %d\nIlość miejsc: %d\nMasa: %d\n", 
      samochod2.model, samochod2.rok_prod, samochod2.ilosc_miejsc, samochod2.masa);

  return 0;
}
```

> **Model: Skoda Fabia II**  
> **Rok produkcji: 2012**  
> **Ilość miejsc: 5**  
> **Masa: 980**  
>
> **Model: Audi A4 B5**  
> **Rok produkcji: 1999**  
> **Ilość miejsc: 5**  
> **Masa: 1100**  





