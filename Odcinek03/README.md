# Kurs C od podstaw

### [#3 - Operacje matematyczne na zmiennych](https://www.youtube.com/watch?v=PolrnlU6c8s&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=4)

<br />

##### Dodawanie

```c
#include <stdio.h>

int main(void) {

  int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 7;

  wynik = a + b; //dodawanie
  printf("%d\n", wynik);

  return 0;
}
```

> **12**

<br />

##### Odejmowanie

```c
#include <stdio.h>

int main(void) {

  int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 7;

  wynik = a - b; //odejmowanie
  printf("%d\n", wynik);

  return 0;
}
```

> **-2**

<br />

##### Mnożenie

```c
#include <stdio.h>

int main(void) {

  int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 7;

  wynik = a * b; //mnożenie
  printf("%d\n", wynik);

  return 0;
}
```

> **35**

<br />

##### Dzielenie

```c
#include <stdio.h>

int main(void) {

  int a = 0, b = 0, wynik = 0; //integer

  a = 5;
  b = 7;

  wynik = a / b; //dzielenie
  printf("%d\n", wynik);

  return 0;
}
```

> **0**
>
> > integer (całkowite) - nie przyjmuje liczb ułamkowych

 <br />

```c
#include <stdio.h>

int main(void) {

  float a = 0.0, b = 0.0, wynik = 0.0; //float

  a = 5.0;
  b = 7.0;

  wynik = a / b; //dzielenie
  printf("%f\n", wynik);

  return 0;
}
```

> **0.714286**
>
> > float

<br />

##### Modulo

```c
#include <stdio.h>

int main(void) {

  int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 7;

  wynik = b % a; //modulo (większa liczba przez mniejszą !!!)
  printf("%d\n", wynik);

  return 0;
}
```

> **2**
>
> > działa tylko na typie **int** i pochodnych **short**, **long** itp

<br />

##### Inkrementacja

```c
#include <stdio.h>

int main(void) {

  int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 7;

  a++; //inkrementacja
  printf("%d\n", a);

  return 0;
}
```

> **6**

<br />

##### Dekrementacja

```c
#include <stdio.h>

int main(void) {

  int a = 0, b = 0, wynik = 0;

  a = 5;
  b = 7;

  a--; //dekrementacja
  printf("%d\n", a);

  return 0;
}
```

> **4**

<br />

##### Operatory przypisania

| Zapis skrócony | Operacja   |
| -------------- | ---------- |
| x = 5          | x = 5      |
| x += 3         | x = x + 3  |
| x -= 3         | x = x - 3  |
| x \*= 3        | x = x \* 3 |
| x /= 3         | x = x / 3  |
| x %= 3         | x = x % 3  |
| x &= 3         | x = x & 3  |
| x \|= 3        | x = x \| 3 |
| x ^= 3         | x = x ^ 3  |
| x >>= 3        | x = x >> 3 |
| x <<= 3        | x = x << 3 |

```c
#include <stdio.h>

int main(void) {

  int a = 0, b = 0, wynik = 0;

  a = 15;
  b = 7;

  a /= 3;
  printf("%d\n", a);

  return 0;
}
```

> **5**
