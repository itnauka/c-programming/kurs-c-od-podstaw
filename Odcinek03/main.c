// Kurs C od podstaw #3 - Operacje matematyczne na zmiennych
// https://www.youtube.com/watch?v=PolrnlU6c8s
// https://www.geeksforgeeks.org/data-types-in-c/

#include <stdio.h>

int main(void) {

  int a = 0, b = 0, wynikDod = 0, wynikOd = 0, wynikMno = 0, wynikMod = 0;
  float wynikDzie = 0.0;

  a = 5;
  b = 7;

  wynikDod = a + b;
  printf("%d\n", wynikDod); // = 12

  wynikOd = a - b;
  printf("%d\n", wynikOd); // = -2

  wynikMno = a * b;
  printf("%d\n", wynikMno); // = 35

  wynikDzie =
      (float)a / (float)b; // wynik mniejszy od 0 trzeba zamienić z int na float
  printf("%f\n", wynikDzie); // = 0.714286

  wynikMod =
      b % a; // modulo działa tylko na typie całkowitej (int, short, long, itp)
  printf("%d\n", wynikMod); // = 2

  a++;               // postinkremetacja zwiększa o 1
  printf("%d\n", a); // = 6

  a--;               // postdekrementacja zmniejsza o 1
  printf("%d\n", a); // = 5

  // ++a - preinkrementacja
  // --a - predekrementacja

  a += 1;            // a = a + 1 lub a++
  a += 2;            // a = a + 2
  printf("%d\n", a); // = 8
  a -= 2;            // a = a - 2
  printf("%d\n", a); // = 6
  a *= 2;            // a = a * 2
  printf("%d\n", a); // = 12
  a /= 2;            // a = a / 2
  printf("%d\n", a); // = 6

  return 0;
}
