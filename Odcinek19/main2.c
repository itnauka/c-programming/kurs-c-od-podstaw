// Kurs C od podstaw #19 - Kalkulator, stringi, geometria, suma ciągu
// https://www.youtube.com/watch?v=ZDoN-IlQlQo
// https://shebang.pl/cpp/programowanie-cpp/r11-wskazniki/
// https://pl.wikibooks.org/wiki/C/Preprocesor

#include <stdio.h>
#define NL "\n"

typedef char* String;

void printCenter(String txt, int width);

int main(void) {

// String text = "Przykładowy tekst";
  printCenter("Przykładowy tekst", 32);
  printf(NL);
  printCenter("Wyśrodkowany przy użyciu", 32);
  printf(NL);
  printCenter("Samodzielnie stworzonej funkcji", 32);
  printf(NL);

  return 0;
}

void printCenter(String txt, int width) {
  int size = 0;
  char *ptr = txt;
  while (*ptr) {
    size++;
    ptr++;
  }

  for (int i = 0; i < (width - size) / 2; i++) {
    printf(" ");
  }
  printf("%s", txt);
}