// Kurs C od podstaw #19 - Kalkulator, stringi, geometria, suma ciągu
// https://www.youtube.com/watch?v=ZDoN-IlQlQo
// https://pl.spoj.com/problems/SUMAN/

#include <stdio.h>

int isPrime(int x);

int main(void) {

  int n = 0;
  scanf("%d", &n);
  int y[n];

  for (int i = 0; i < n; i++) {
    scanf("%d", &y[i]);
  }

  for (int i = 0; i < n; i++) {
    if (isPrime(y[i])) {
      printf("TAK\n");
    } else {
      printf("NIE\n");
    }
  }

  return 0;
}

inline int isPrime(int x) {
  if (x < 2) {
    return 0;
  }
  for (int i = 2; i * i <= x; i++) {
    if(x % i == 0) return 0;
  }
  return 1;
}


