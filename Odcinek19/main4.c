// Kurs C od podstaw #19 - Kalkulator, stringi, geometria, suma ciągu
// https://www.youtube.com/watch?v=ZDoN-IlQlQo
// https://pl.spoj.com/problems/SUMAN/

#include <stdio.h>


int main(void) {

  int input[10];
  long int wynik = 0;

  for (int i = 0; i < 10; i++) {
    scanf("%d", &input[i]);
  }

  for (int i = 0; i < 10; i++) {
    for (int j = 0; j < input[i]; j++) {
      wynik += j + 1;
    }
    printf("%ld\n", wynik);
    wynik = 0;
  }

  return 0;
}