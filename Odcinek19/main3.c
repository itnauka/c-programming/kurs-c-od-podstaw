// Kurs C od podstaw #19 - Kalkulator, stringi, geometria, suma ciągu
// https://www.youtube.com/watch?v=ZDoN-IlQlQo
// https://pl.spoj.com/problems/SUMAN/

#include <stdio.h>

#define PI 3.1415926654

int main(void) {

  double r = 0.0, d = 0.0, wynik = 0.0;

  scanf("%lf %lf", &r, &d);

  wynik  = PI * ((r * r) - (0.25 * d * d));
  printf("%.2lf", wynik);

  return 0;
}