// Kurs C od podstaw #19 - Kalkulator, stringi, geometria, suma ciągu
// https://www.youtube.com/watch?v=ZDoN-IlQlQo
// https://pl.wikibooks.org/wiki/C/Preprocesor

#include <stdio.h>

int main(void) {

  double var1, var2, wynik;
  char znak;

  printf("KALKULATOR\n\n");

  printf("Podaj liczbę: ");
  scanf("%lf", &var1);

  printf("\nPodaj odpowiedni znak:\n+ : dodawanie\n- : odejmowanie\n* : "
         "mnożenie\n/ : dzielenie\n");
  printf("Wybrany znak to: ");
  scanf("%s", &znak);

  printf("\nPodaj liczbę: ");
  scanf("%lf", &var2);
  printf("\n------------------");

  switch (znak) {
  case '+':
    wynik = var1 + var2;
    break;
  case '-':
    wynik = var1 - var2;
    break;
  case '*':
    wynik = var1 * var2;
    break;
  case '/':
    wynik = var1 / var2;
    break;
  }
  printf("\nWynik: %lf\n", wynik);

  return 0;
}
