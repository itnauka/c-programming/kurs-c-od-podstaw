# Kurs C od podstaw

### [#19 - Kalkulator, stringi, geometria, suma ciągu](https://www.youtube.com/watch?v=ZDoN-IlQlQo&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=20)

#### Kalkulator

```c
#include <stdio.h>

int main(void) {

  double var1, var2, wynik;
  char znak;

  printf("KALKULATOR\n\n");

  printf("Podaj liczbę: ");
  scanf("%lf", &var1);

  printf("\nPodaj odpowiedni znak:\n+ : dodawanie\n- : odejmowanie\n* : "
         "mnożenie\n/ : dzielenie\n");
  printf("Wybrany znak to: ");
  scanf("%s", &znak);

  printf("\nPodaj liczbę: ");
  scanf("%lf", &var2);
  printf("\n------------------");

  switch (znak) {
  case '+':
    wynik = var1 + var2;
    break;
  case '-':
    wynik = var1 - var2;
    break;
  case '*':
    wynik = var1 * var2;
    break;
  case '/':
    wynik = var1 / var2;
    break;
  }
  printf("\nWynik: %lf\n", wynik);

  return 0;
}
```

> **KALKULATOR** 
> 
> **Podaj liczbę: 3**  
> 
> **Podaj odpowiedni znak:**  
> **+ : dodawanie**  
> **- : odejmowanie**  
> **: mnożenie**  
> **/ : dzielenie**  
> **Wybrany znak to: \***  
> 
> **Podaj liczbę: 6**  
>
> **\--------------------------**  
> **Wynik: 18.000000**  

<br />

#### Stringi

```c
#include <stdio.h>
#define NL "\n"

typedef char* String;

void printCenter(String txt, int width);

int main(void) {

// String text = "Przykładowy tekst";
  printCenter("Przykładowy tekst", 32);
  printf(NL);
  printCenter("Wyśrodkowany przy użyciu", 32);
  printf(NL);
  printCenter("Samodzielnie stworzonej funkcji", 32);
  printf(NL);

  return 0;
}

void printCenter(String txt, int width) {
  int size = 0;
  char *ptr = txt;
  while (*ptr) {
    size++;
    ptr++;
  }

  for (int i = 0; i < (width - size) / 2; i++) {
    printf(" ");
  }
  printf("%s", txt);
}
```

> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **Przykładowy tekst**  
> &nbsp;&nbsp;&nbsp; **Wyśrodkowany przy użyciu**  
> **Samodzielnie stworzonej funkcji**  

<br />

#### [Geometria](https://pl.spoj.com/problems/ETI06F1/ "Odnośnik do zadania")


```c
#include <stdio.h>

#define PI 3.1415926654

int main(void) {

  double r = 0.0, d = 0.0, wynik = 0.0;

  scanf("%lf %lf", &r, &d);

  wynik  = PI * ((r * r) - (0.25 * d * d));
  printf("%.2lf", wynik);

  return 0;
}
```

> **4**
> **7**
> **11.78**  

<br />

#### [SUMAN - Suma](https://pl.spoj.com/problems/SUMAN/ "Odnośnik do zadania")

```c
#include <stdio.h>

int main(void) {

  int input[10];
  long int wynik = 0;

  for (int i = 0; i < 10; i++) {
    scanf("%d", &input[i]);
  }

  for (int i = 0; i < 10; i++) {
    for (int j = 0; j < input[i]; j++) {
      wynik += j + 1;
    }
    printf("%ld\n", wynik);
    wynik = 0;
  }

  return 0;
}
```

> **9**  
> **6**  
> **70**  
> **40**  
> **30**  
> **4**  
> **6**  
> **7**  
> **9**  
> **8**  
> **45**  
> **21**  
> **2485**  
> **820**  
> **465**  
> **10**  
> **21**  
> **28**  
> **45**  
> **36**  

<br />

#### [Liczby Pierwsze](https://pl.spoj.com/problems/PRIME_T/ "Odnośnik do zadania")

```c
#include <stdio.h>

int isPrime(int x);

int main(void) {

  int n = 0;
  scanf("%d", &n);
  int y[n];

  for (int i = 0; i < n; i++) {
    scanf("%d", &y[i]);
  }

  for (int i = 0; i < n; i++) {
    if (isPrime(y[i])) {
      printf("TAK\n");
    } else {
      printf("NIE\n");
    }
  }

  return 0;
}

inline int isPrime(int x) {
  if (x < 2) {
    return 0;
  }
  for (int i = 2; i * i <= x; i++) {
    if(x % i == 0) return 0;
  }
  return 1;
}
```

> **5**  
> **7**  
> **4**  
> **9**  
> **5**  
> **0**  
> **TAK**  
> **NIE**  
> **NIE**  
> **TAK**  
> **NIE**  




