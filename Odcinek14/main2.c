// Kurs C od podstaw #14 - Wskaźniki
// https://www.youtube.com/watch?v=yptUMe6oX4c
// https://pl.wikibooks.org/wiki/C/Wska%C5%BAniki

#include <stdio.h>

// rozmiar w bajtach:
int a;            // 4
short int b;      // 2
long int c;       // 8
long long int d;  // 8
char e;           // 1

float f;          // 4
double g;         // 8

char *wsk1;
short int *wsk2;
int *wsk3;
long int *wsk4;
long long int *wsk5;

short int tab1[] = { 43, 123, 256, 689, 1300, 15200 };
   //short int tab1[] = { 0x002B, 0x007B, 0x0100, 0x02B1, 0x0514, 0x3B60 }

char tab2[] = { 10, 32, 65, 154, 200, 252 };
   //char tab2[] = { 0x0A, 0x20, 0x41, 0x9A, 0xC8, 0xFC }

void rozmiar_typ(void);

int main(void) {

rozmiar_typ();

  printf("---------------------------\n");

  a = 10;                       //przypisanie wartości
  wsk3 = &a;                    //przypisanie adresu zmiennej
  *wsk3 += 5;                   //dodanie do zmiennej 'a' liczby poprzez wskaźnik
  printf("%d\n", a);

  printf("---------------------------\n");

  wsk1 = (char *) &tab1;//przypisanie adresu tabeli, adres zaczyna się od 1 komórki
  wsk1 += 2;
  printf("%d\n", *wsk1);

  printf("---------------------------\n");

  wsk2 = (short int *)&tab2; //przypisanie adresu tabeli, adres zaczyna się od 1 komórki
  wsk2 += 1;//<--
  printf("%d\n", *wsk2);


  return 0;
}

// Funkcja wyświetla rozmiar różnych typów zmiennych w bajtach
void rozmiar_typ ( void ) {
  printf("%d %d %d %d %d %d %d\n", sizeof(a), sizeof(b), sizeof(c), sizeof(d), sizeof(e), sizeof(f), sizeof(g));
}