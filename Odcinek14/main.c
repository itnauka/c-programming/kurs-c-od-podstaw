// Kurs C od podstaw #14 - Wskaźniki
// https://www.youtube.com/watch?v=yptUMe6oX4c
// https://pl.wikibooks.org/wiki/C/Wska%C5%BAniki

#include <stdio.h>

int a=8768, b=6587, c=456985;

int *wsk1 = &a;
int *wsk2 = &b;
//dopuszczalne zapisy wskaźników
// int * wsk2 = &b;
// int* wsk2 = &b;

int main(void) {

  printf("%d\n", (int)wsk1);

  printf("%d\n", (int)wsk2);

  printf("%d\n", *wsk1); // wsk1 = 8768

  printf("%d\n", *wsk2); // wsk2 = 6587

  printf("--------------\n");

  printf("%d\n", *wsk1); //wsk1 = 8768
  wsk1++;
  printf("%d\n", *wsk1); //wsk1 = 6587
  wsk1++;
  printf("%d\n", *wsk1); //wsk1 = 456985

  printf("--------------\n");

  a=8768;
  int *wsk1 = &a;
  printf("%d\n", (int)sizeof(*wsk1)); //rozmiar zmiennej na którą wskazuje wskaźnik (czyli (a))
  printf("%d\n", (int)sizeof(wsk1)); //rozmiar adresu (a)

  return 0;
}
