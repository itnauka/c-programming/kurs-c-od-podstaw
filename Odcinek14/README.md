# Kurs C od podstaw

### [#14 - Wskaźniki](https://www.youtube.com/watch?v=yptUMe6oX4c&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=15)

<br />

```c
#include <stdio.h>

int a=8768, b=6587;

int *wsk1 = &a;
int *wsk2 = &b;

//dopuszczalne zapisy wskaźników
// int * wsk2 = &b;
// int* wsk2 = &b;

int main(void) {

  printf("%d\n", (int)wsk1);

  printf("%d\n", (int)wsk2);

  return 0;
}

```

> numer adresu rzutowany na intiger
>> **1863942160**  
>> **1863942164**  

<br />

```c
#include <stdio.h>

int a=8768, b=6587;

int *wsk1 = &a;
int *wsk2 = &b;

int main(void) {

  printf("%d\n", *wsk1);

  printf("%d\n", *wsk2);

  return 0;
}

```

> zwraca wartość na którą wskazuje wskaźnik
>> **8768**  
>> **6587**  

<br />

```c
#include <stdio.h>

int a=8768, b=6587, c=456985;

int *wsk1 = &a;

int main(void) {

  printf("%d\n", *wsk1);
  wsk1++;//przesunięcie o 4 bajty
  printf("%d\n", *wsk1);
  wsk1++;//przesunięcie o kolejne 4 bajty
  printf("%d\n", *wsk1);

  return 0;
}

```

> zwraca wartość na którą wskazuje wskaźnik
>> **8768**  
>> **6587**  
>> **456985**  

<br />

```c
#include <stdio.h>

int a=8768;

int *wsk1 = &a;

int main(void) {

//rozmiar zmiennej na którą wskazuje wskaźnik (czyli (a))
  printf("%d\n", (int)sizeof(*wsk1));

//rozmiar adresu (a)
  printf("%d\n", (int)sizeof(wsk1));

  return 0;
}

```

> zwraca wartość na którą wskazuje wskaźnik
>> **4**  
>> **8**  

<br />

#### Rozmiary typów danych w bajtach

| Typ danych      | Rozmiar (w bajtach) |
|-----------------|----------------------|
| int             | 4                    |
| short int       | 2                    |
| long int        | 8                    |
| long long int   | 8                    |
| char            | 1                    |

<br />

#### Rozmiary typów danych zmiennoprzecinkowych w bajtach

| Typ danych      | Rozmiar (w bajtach) |
|-----------------|----------------------|
| float           | 4                    |
| double          | 8                    |

<br />

```c
#include <stdio.h>

void rozmiar_typ(void);

int main(void) {

  rozmiar_typ();

  return 0;
}

void rozmiar_typ ( void ) {
  printf("%d %d %d %d %d %d %d\n", sizeof(a), sizeof(b), sizeof(c), sizeof(d), sizeof(e), sizeof(f), sizeof(g));
}
```

> **15**

<br />

|          short int tab1[] = { 43, 123, 256, 689, 1300, 15200 };            |
|:--------------------------------------------------------------------------:|
| **short int tab1[] = { 0x002B, 0x007B, 0x0100, 0x02B1, 0x0514, 0x3B60 }**  |
| **char tab1[] = { 0x00 2B, 0x00 7B, 0x01 00, 0x02 B1, 0x05 14, 0x3B 60 }** |


|       char tab2[] = { 10, 32, 65, 154, 200, 252 };       |
|:--------------------------------------------------------:|
| **char tab2[] = { 0x0A, 0x20, 0x41, 0x9A, 0xC8, 0xFC }** |

<br />

```c
#include <stdio.h>

char e;
char *wsk1;

short int tab1[] = { 43, 123, 256, 689, 1300, 15200 };
//short int tab1[] = { 0x002B, 0x007B, 0x0100, 0x02B1, 0x0514, 0x3B60 }

char tab2[] = { 10, 32, 65, 154, 200, 252 };
//char tab2[] = { 0x0A, 0x20, 0x41, 0x9A, 0xC8, 0xFC }

int main(void) {

  wsk1 = (char *)&tab1; //przypisanie adresu tabeli, adres zaczyna się od 1 komórki
  wsk1 += 0;//<--
  printf("%d\n", *wsk1);

  return 0;
}
```

> **43**

<br />

```c
#include <stdio.h>

char e;
char *wsk1;

short int tab1[] = { 43, 123, 256, 689, 1300, 15200 };
//short int tab1[] = { 0x002B, 0x007B, 0x0100, 0x02B1, 0x0514, 0x3B60 }

char tab2[] = { 10, 32, 65, 154, 200, 252 };
//char tab2[] = { 0x0A, 0x20, 0x41, 0x9A, 0xC8, 0xFC }

int main(void) {

  wsk1 = (char *)&tab1; //przypisanie adresu tabeli, adres zaczyna się od 1 komórki
  wsk1 += 1;//<--
  printf("%d\n", *wsk1);

  return 0;
}
```

> **0**

<br />

```c
#include <stdio.h>

short int tab1[] = { 43, 123, 256, 689, 1300, 15200 };
//short int tab1[] = { 0x002B, 0x007B, 0x0100, 0x02B1, 0x0514, 0x3B60 }

char tab2[] = { 10, 32, 65, 154, 200, 252 };
//char tab2[] = { 0x0A, 0x20, 0x41, 0x9A, 0xC8, 0xFC }

short int b;
short int *wsk2;

int main(void) {

  wsk2 = (short int *)&tab2; //przypisanie adresu tabeli, adres zaczyna się od 1 komórki
  wsk2 += 0;//<--
  printf("%d\n", *wsk2);

  return 0;
}
```

> **8202**

<br />

```c
#include <stdio.h>

short int tab1[] = { 43, 123, 256, 689, 1300, 15200 };
//short int tab1[] = { 0x002B, 0x007B, 0x0100, 0x02B1, 0x0514, 0x3B60 }

char tab2[] = { 10, 32, 65, 154, 200, 252 };
//char tab2[] = { 0x0A, 0x20, 0x41, 0x9A, 0xC8, 0xFC }

short int b;
short int *wsk2;

int main(void) {

  wsk2 = (short int *)&tab2; //przypisanie adresu tabeli, adres zaczyna się od 1 komórki
  wsk2 += 1;//<--
  printf("%d\n", *wsk2);

  return 0;
}
```

> **-26047**

