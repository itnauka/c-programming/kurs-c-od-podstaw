# Kurs C od podstaw

### [#10 - Instrukcja switch...case](https://www.youtube.com/watch?v=q7MdcsRdI3Y&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=12)

<br />

```c
#include <sthio.h>

int main(void) {

  int a = 0;
  scanf("%d", &a);

/*
  if (a == 0) printf("ZERO\n");
  else if (a == 1) printf("JEDEN\n");
  else if (a == 2) printf("DWA\n");
  else if (a == 3) printf("TRZY\n");
  else printf("INNA LICZBA\n");
*/

switch (a) {
  case 0: printf("ZERO\n");
  case 1: printf("JEDEN\n");
  case 2: printf("DWA\n");
  case 3: printf("TRZY\n");
  default: printf("INNA LICZBA\n");
}

switch (a) {
  case 0: printf("ZERO\n"); break;
  case 1: printf("JEDEN\n"); break;
  case 2: printf("DWA\n"); break;
  case 3: printf("TRZY\n");break;
  default: printf("INNA LICZBA\n");//DEFAULT: działa jak ELSE
}

  return 0;
}
```

> **Podaj cyfrę: 2**  
> **DWA**  
> **TRZY**  
> **INNA LICZBA**  
>
> **==========================**  
>
> **DWA**



