// Kurs C od podstaw #10 - Instrukcja switch...case
// https://www.youtube.com/watch?v=q7MdcsRdI3Y

#include <stdio.h>

int main(void) {

  int a = 0;
  printf("Podaj cyfrę: ");
  scanf("%d", &a);

/*
  if (a == 0) printf("ZERO\n");
  else if (a == 1) printf("JEDEN\n");
  else if (a == 2) printf("DWA\n");
  else if (a == 3) printf("TRZY\n");
  else printf("INNA LICZBA\n");
*/

switch (a) {
  case 0: printf("ZERO\n");
  case 1: printf("JEDEN\n");
  case 2: printf("DWA\n");
  case 3: printf("TRZY\n");
  default: printf("INNA LICZBA\n");
}

  printf("\n");
  printf("==========================\n");
  printf("\n");

switch (a) {
  case 0: printf("ZERO\n"); break;
  case 1: printf("JEDEN\n"); break;
  case 2: printf("DWA\n"); break;
  case 3: printf("TRZY\n");break;
  default: printf("INNA LICZBA\n");//DEFAULT
}

  return 0;
}
