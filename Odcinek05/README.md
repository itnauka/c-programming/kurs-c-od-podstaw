# Kurs C od podstaw

### [#5 - Instrukcja warunkowa if else](https://www.youtube.com/watch?v=cTJvPvOlc_0&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=6)

<br />

```c
#include <stdio.h>

int main(void) {

  int a;

  printf("Podaj liczbę: ");
  scanf("%d", &a);

  if (a > 0) {
    printf("Liczba dodatnia\n");
  }

  return 0;
}
```

> **Podaj liczbę: 6**   
> **Liczba dodatnia**

<br />

```c
#include <stdio.h>

int main(void) {

  int a;

  printf("Podaj liczbę: ");
  scanf("%d", &a);

  if (a > 0) {
    printf("Liczba dodatnia\n");
  }

  return 0;
}
```

> **Podaj liczbę: -1**
>> Brak informacji ponieważ a jest mniejsza od 0

<br />

```c
#include <stdio.h>

int main(void) {

  int a;

  printf("Podaj liczbę: ");
  scanf("%d", &a);

  if (a > 0) {
    printf("Liczba dodatnia\n");
  }

  else if ( a < 0) {
    printf("Liczba jest mnijsza od 0");
  }

  else {
  printf("ZERO");
  }


  return 0;
}
```

> **Podaj liczbę: -1**   
> **Liczba jest mnijsza od 0**

<br />

```c
#include <stdio.h>

int main(void) {

  int a;

  printf("Podaj liczbę: ");
  scanf("%d", &a);

  if (a > 0) {
    printf("Liczba dodatnia\n");
  }

  else if ( a < 0) {
    printf("Liczba jest mnijsza od 0\n");
  }

  else {
  printf("ZERO\n");
  }


  return 0;
}
```

> **Podaj liczbę: 0**  
> **ZERO**

<br />

```c
#include <stdio.h>

int main(void) {

  int a;

  printf("Podaj wiek: ");
  scanf("%d", &a);

  if ((a > 18) || (a = 18)) { // to samo co ( a >= 18 )
    printf("Osoba pełnoletnia, może pić wódkę\n");
  }
  else {
    printf("Osoba niepełnoletnia\n");
  }

  return 0;
}
```

> **Podaj wiek: 19**  
> **Osoba pełnoletnia, może pić wódkę**

<br />

```c
#include <stdio.h>

int main(void) {

  int a;

  printf("Podaj wiek: ");
  scanf("%d", &a);

  if ((a > 18) || (a = 18)) {
    printf("Osoba pełnoletnia, może pić wódkę\n");
  }
  else {
    printf("Osoba niepełnoletnia\n");
  }

  return 0;
}
```

> **Podaj wiek: 18**  
> **Osoba pełnoletnia, może pić wódkę**

<br />

```c
#include <stdio.h>

int main(void) {

  int a;

  printf("Podaj wiek: ");
  scanf("%d", &a);

  if ((a > 18) || (a = 18)) {
    printf("Osoba pełnoletnia, może pić wódkę\n");
  }
  else {
    printf("Osoba niepełnoletnia\n");
  }

  return 0;
}
```

> **Podaj wiek: 17**  
> **Osoba niepełnoletnia**

<br />

##### jeżeli pierwszy się nie wykona, to przechodzi do następnego

```
//SKŁADNIA
if (warunek) {
  polecenie
} else if (warunek) {
  polecenie
} else
  polecenie
```

##### wykonywane niezależnie

```
//SKŁADNIA
if (warunek) {
  polecenie
}
if (warunek) {
  polecenie
} else
  polecenie
```