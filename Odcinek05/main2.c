// Kurs C od podstaw #5 - Instrukcja warunkowa if else
// https://www.youtube.com/watch?v=cTJvP
// https://pl.wikibooks.org/wiki/C/Operatory
// https://slideplayer.pl/slide/2905575/10/images/5/BRAMKI+LOGICZNE.jpg
// https://www.programiz.com/c-programming/c-operators

#include <stdio.h>

int main(void) {

  int a;

  printf("Podaj wiek: ");
  scanf("%d", &a);

  if ((a > 18) || (a = 18)) { // to samo co ( a >= 18 )
    printf("Osoba pełnoletnia, może pić wódkę\n");
  }

  else {
    printf("Osoba niepełnoletnia\n");
  }

  return 0;
}
