// Kurs C od podstaw #5 - Instrukcja warunkowa if else
// https://www.youtube.com/watch?v=cTJvP
// https://pl.wikibooks.org/wiki/C/Operatory
// https://slideplayer.pl/slide/2905575/10/images/5/BRAMKI+LOGICZNE.jpg
// https://www.programiz.com/c-programming/c-operators

#include <stdio.h>


int main(void) {

  int a;

  printf("Podaj liczbę: ");
  scanf("%d", &a);

  if (a > 0) {
    printf("Liczba dodatnia\n");
  }

  else if ( a < 0) {
    printf("Liczba jest mnijsza od 0\n");
  }

  else {
  printf("ZERO\n");
  }


  return 0;
}
