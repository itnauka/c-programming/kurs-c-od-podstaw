# Kurs C od podstaw

### [#15 - Znaki i ciągi znaków (string)](https://www.youtube.com/watch?v=lWeIq7cEZok&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=16)

<br />

#### Character

```c
#include <stdio.h>

char znak1 = 'A';
char znak2 = 65;
char znak3 = 'w';
char znak4 = 64;
char znak5 = 201;//ASCII poza zakresem
char znak6 = 202;//ASCII poza zakresem

int main(void) {

  printf("%c\n", znak1);
  printf("%c\n", znak2);
  printf("%c\n", znak3);
  printf("%c\n", znak4);
  printf("%c\n", znak5);
  printf("%c\n", znak6);

  return 0;
}
```

> **A**  
> **A**  
> **w**  
> **@**  
> **�** - ASCII poza zakresem !  
> **�** - ASCII poza zakresem !  

<br />

#### String

```c
#include <stdio.h>

char *znak = "Kurs programowania";

int main(void) {

  printf("%c", *znak);
  znak++;
  printf("%c", *znak);
  znak++;
  printf("%c", *znak);
  znak++;
  printf("%c", *znak);
  znak++;
  printf("%c", *znak);
  znak++;
  printf("%c", *znak);

  return 0;
}
```

> **Kurs p**

<br />

#### Wyświetlanie napisu "Kurs programowania" za pomocą pętli while

```c
#include <stdio.h>

char *znak = "Kurs programowania";

int main(void) {

  while (*znak != NULL) {
    printf("%c", *znak++);
    // znak++;
  }

  return 0;
}
```

> **Kurs programowania**

<br />

```c
#include <stdio.h>

char *znak = "Kurs programowania w języku C";

int main(void) {

  while (*znak) printf("%c", *znak++);

  return 0;
}
```

> **Kurs programowania w języku C**

<br />

#### typedef - tworzenie nowego typu danych (który symuluje inny typ)

```c
#include <stdio.h>

typedef char* String;
String napis = "Kurs programowania w języku C";

int main(void) {

  printf("%s", napis);

  return 0;
}
```

> **Kurs programowania w języku C**
