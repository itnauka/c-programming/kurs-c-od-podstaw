// Kurs C od podstaw #15 - Znaki i ciągi znaków (string)
// https://www.youtube.com/watch?v=lWeIq7cEZok
// https://www.bryk.pl/wypracowania/pozostale/informatyka/15912-tabela-znakow-w-standardowych-kodach-ascii.html

#include <stdio.h>

// ASCII
char znak1 = 'A';
char znak2 = 65;
char znak3 = 'w';
char znak4 = 64;
char znak5 = 201;
char znak6 = 202;

// string
char *znak7 = "Kurs programowania";

int main(void) {

  printf("%c\n", znak1);
  printf("%c\n", znak2);
  printf("%c\n", znak3);
  printf("%c\n", znak4);
  printf("%c\n", znak5);
  printf("%c\n", znak6);

  printf("-----------------\n");

// wyświetlanie napisu ""Kurs programowania""
  printf("%c", *znak7); // string
  znak7++;
  printf("%c", *znak7);
  znak7++;
  printf("%c", *znak7);
  znak7++;
  printf("%c", *znak7);
  znak7++;
  printf("%c", *znak7);
  znak7++;
  printf("%c", *znak7);

  printf("\n-----------------\n");

// wyświetlanie napisu "Kurs programowania" za pomocą pętli while
  znak7 = "Kurs programowania";
  while (*znak7 != NULL) {
    printf("%c", *znak7++);
    // znak7++;
  }

  printf("\n-----------------\n");

// wyświetlanie napisu "Kurs programowania" za pomocą pętli while z dodaniem tekstu do zmiennej
  znak7 = "Kurs programowania w języku C";
  while (*znak7) printf("%c", *znak7++);

  printf("\n-----------------\n");

// STRINGI
  typedef char* String; //tworzenie nowy typ danych który symuluje jakiś inny typ np. String
  String napis = "Kurs C od podstaw #15 - Znaki i ciągi znaków (string)";
  printf("%s", napis);

  return 0;
}
