// Kurs C od podstaw #2 - Rzutowanie zmiennych
// https://www.youtube.com/watch?v=sCPBYlo5-dI
// https://www.geeksforgeeks.org/data-types-in-c/ 

#include <stdio.h>

int main(void)
{

  float PI = 3.14159;
  float wynik = 0.0; // zawsze wpisujemy 0.0

  wynik = (int)(2 * PI); //rzutowanie z float (wynik) na int


  printf("Wartość wyniku %f \n", wynik);

  return 0;
}
