# Kurs C od podstaw

### [#2 - Rzutowanie zmiennych](https://www.youtube.com/watch?v=sCPBYlo5-dI&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=3)

<br />

```c
#include <stdio.h>

int main(void) {

  float PI = 3.14159;
  float wynik = 0.0; // zawsze wpisujemy 0.0

  wynik = 2 * PI;


  printf("Wartość wyniku %f \n", wynik);

  return 0;
}
```

>**Wartość wyniku 6.283180**

<br />

```c
#include <stdio.h>

int main(void) {

  float PI = 3.14159;
  float wynik = 0.0;

  wynik = (int)(2 * PI); //rzutowanie z float (wynik) na int


  printf("Wartość wyniku %f \n", wynik);

  return 0;
}
```

>**Wartość wyniku 6.000000**
