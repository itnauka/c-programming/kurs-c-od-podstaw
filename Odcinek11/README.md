# Kurs C od podstaw

### [#11 - Zakres widoczności zmiennych](https://www.youtube.com/watch?v=--DPGzUbfOo&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=12)

<br />

```c
#include <complex.h>
#include <stdio.h>

//zmienna globalna widoczna w całym programie
int globalna = 0;

int main(void) {

  // zmienna lokalna widoczna tylko w funkcji main
  int lokalna = 0;

  for (int i = 0; i < 20; i++) {
  printf("%d ", i);
  lokalna += i;
  }
  printf("\nSuma zmiennej \"lokalna\" to: %d", lokalna);
  // printf("\n%d", i); //poza pętlą, program nie widzi zmiennej "i"

  return 0;
}
```

> **0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19**  
> **Suma zmiennej "lokalna" to: 190%**  

<br />

```c
#include <complex.h>
#include <stdio.h>

int main(void) {

  int lokalna = 0;

  for (int i = 0; i < 20; i++) {
    printf("%d ", i);
    lokalna += i;
  }
  printf("\n%d", i); // poza pętlą, program nie widzi zmiennej "i"

  return 0;
}
```

> **main.c: In function ‘main’:**  
> **main.c:23:18: error: ‘i’ undeclared (first use in this function)**  
>    **23 |   printf("\n%d", i); // poza pętlą, program nie widzi zmiennej "i"**  
>       **&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;^**  
> **main.c:23:18: note: each undeclared identifier is reported only once for each function it appears in**


