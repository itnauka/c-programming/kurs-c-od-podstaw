// Kurs C od podstaw #11 - Zakres widoczności zmiennych
// https://www.youtube.com/watch?v=--DPGzUbfOo
// https://pl.wikibooks.org/wiki/C/Operatory
// https://slideplayer.pl/slide/2905575/10/images/5/BRAMKI+LOGICZNE.jpg
// https://www.programiz.com/c-programming/c-operators

#include <complex.h>
#include <stdio.h>

// zmienna globalna widoczna w całym programie
int globalna = 0;

int main(void) {

  // zmienna lokalna widoczna tylko w funkcji main
  int lokalna = 0;

  for (int i = 0; i < 20; i++) {
    printf("%d ", i);
    lokalna += i;
  }
  printf("\nSuma zmiennej \"lokalna\" to: %d", lokalna);
  // printf("\n%d", i); // poza pętlą, program nie widzi zmiennej "i"

  return 0;
}
