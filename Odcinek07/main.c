// Kurs C od podstaw #7 - Pętla while oraz do...while
// https://www.youtube.com/watch?v=DhAUzyaJzeY

#include <stdio.h>

int main(void) {

  int a = 0;

  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);

  printf("\n");
  printf("=====================\n");
  printf("\n");

  a = 0;
  int b = 0;
  printf("Podaj liczbę do której ma wypisywać: ");
  scanf("%d", &b);

  while (a < (b + 1)) { // lub ( a < 11 )
    printf("%d\n", a);
    a++;
  }

  printf("\n");
  printf("---------------------\n");
  printf("\n");

  a = 0;

  do {
    printf("%d\n", a);
    a++;
  } while (a < b);

  return 0;
}
