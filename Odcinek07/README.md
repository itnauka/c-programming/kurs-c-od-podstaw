# Kurs C od podstaw

### [#7 - Pętla while oraz do...while](https://www.youtube.com/watch?v=DhAUzyaJzeY&list=PL0oxIZsFGA4EAxDvx1PNzNAlr8g4xi3XP&index=8)

<br />

```c
#include <stdio.h>

int main(void) {

  int a = 0;

  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);
  a++;
  printf("%d\n", a);

  return 0;
}
```

> **0**  
> **1**  
> **2**  
> **3**  
> **4**  
> **5**  
> **6**  
> **7**  
> **8**  
> **9**  
> **10**

<br />

```c
#include <stdio.h>

int main(void) {

  int a = 0;

  while (a <= 10)
    printf("%d\n", a);
    a++;
  }

  return 0;
}
```

> **0**  
> **1**  
> **2**  
> **3**  
> **4**  
> **5**  
> **6**  
> **7**  
> **8**  
> **9**  
> **10**

<br />

```c
#include <stdio.h>

int main(void) {

  int a = 0, b = 0;
  scanf("%d", &b);

  while (a < b)
    printf("%d\n", a);
    a++;
  }

  return 0;
}
```

> **6**  
> **0**  
> **1**  
> **2**  
> **3**  
> **4**  
> **5**  

<br />

```c
#include <stdio.h>

int main(void) {

  int a = 0, b = 0;
  
  printf("Podaj liczbę do której ma wypisywać: ");
  scanf("%d", &b);

  while (a < (b + 1))
    printf("%d\n", a);
    a++;
  }

  return 0;
}
```

> **Podaj liczbę do której ma wypisywać: 6**  
> **0**  
> **1**  
> **2**  
> **3**  
> **4**  
> **5**  
> **6**

<br />

```c
#include <stdio.h>

int main(void) {

  int a = 0, b = 0;
  printf("Podaj liczbę do której ma wypisywać: ");
  scanf("%d", &b);

  while (a < b)
    printf("%d\n", a);
    a++;
  }

  return 0;
}
```

> **Podaj liczbę do której ma wypisywać: 0**

<br />

```c
#include <stdio.h>

int main(void) {

  int a = 0, b = 0;
  printf("Podaj liczbę do której ma wypisywać: ");
  scanf("%d", &b);

  do {
    printf("%d\n", a);
    a++;
  } while (a < b);

  return 0;
}
```

> **Podaj liczbę do której ma wypisywać: 0**  
> **0**  

<br />

<img src="https://khalidabuhakmeh.com/assets/images/posts/road-runner/meme.jpg" alt="Difference between while loop and do-while loop. As explained by Roadrunner and Wile E. Coyote" width="600">

